import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class SimaService { 

    private api = 'https://sima.unsch.edu.pe';
    private token = 'eyJhbGciOiJSUzI1NiJ9.eyJ1c2VybmFtZSI6ImhldmVyIn0.P8iwp1bZ6iyYo_0UgLWrmUyBz4VTqL4x7KhljzmW1jp0nGBpXmPLUOrZT9-wca5AEWpBM6pOCItIc1if99aZlyob2xOavlHvEkFfi1UrtA4Uzkgebtxr-FQ4cgH27AtkcVCuG1hu6zzfz9ncRUFyxLqtCw1dy83bBeA45-pvwFvrvU1BN7BZMHhzYC940PUEKogayyl2cLF-93NxKdLX0RGUAMuLmRgveCCtiAPakH4IDVYX55_ikH9i2aeUnCHlr3wguiTnh6NXHullVzUZSLaH102wqx7pvruoXRMNyVz-ynxdQhWh_DXLvOEviSqJGr5wQFJ9IWB8O-TFFKENNg'

    constructor(
        private http: HttpClient
    ) {

    }
    public getToken(valor): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/api/services/student_tracking/${valor}`, { headers: new HttpHeaders({ 'Authorization':this.token }) });

    }
}
