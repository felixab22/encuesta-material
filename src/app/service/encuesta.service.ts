import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { URL_SERVICIOS } from '../configuration/ip.configuration';
import { EncuestaModel, PreguntaModel, CargaAcademicaModel, EncuestadorModel, DocentesModel, CursosModel, PersonalModel, GraficandoModel, GraficaModel, QuestionsResponseModel, ComentarioModel, CursosListModel } from '../model/encuesta.model';


@Injectable({
    providedIn: 'root'
})
export class EncuestaService {
    api = URL_SERVICIOS;
   

    constructor(
        private http: HttpClient
    ) {

    }
    // ========================== Encuesta ==============================
    // listar
    public getAllEncuesta(): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/encuesta/getAllEncuesta`);

    }
    // Guardar y editar
    public saveOrUpdateEncuesta(encuesta: EncuestaModel): Observable<EncuestaModel> {
        return this.http.post<EncuestaModel>(this.api + `/encuesta/saveEncuesta`, JSON.stringify(encuesta), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    public saveOrUpdateEncuestaPregunta(encuesta: any): Observable<any> {
        return this.http.post<any>(this.api + `/encuesta/saveEncuestaPregunta`, encuesta, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // ========================== Preguntas ==============================
    // listar
    // public getAllPregunta(encuesta): Observable<any[]> {
    //     return this.http.get<any[]>(this.api + `/pregunta/getAllPreguntaByEncuesta?idencuesta=${encuesta}`);
    // }
    public getAllPreguntaXidencuesta(encuesta): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/pregunta/getAllPreguntasByEncuesta?idencuesta=${encuesta}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    public getAllPreguntas(id): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/pregunta/getAllPregunta?tipopregunta=${id}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // Guardar y editar
    public saveOrUpdatePregunta(pregunta: PreguntaModel): Observable<PreguntaModel> {
        return this.http.post<PreguntaModel>(this.api + `/pregunta/savePregunta`, JSON.stringify(pregunta), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // ========================== Alternativa =============================={}
    public saveOrUpdateAlternativa(alternativa: any): Observable<any> {
        return this.http.post<any>(this.api + `/alternativas/saveOrUpdateAlternativa`, alternativa, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }


    // ========================== Docentes =============================={}
    // listar
    public deleteCargaAcademica(id: number): void {
        this.http.post(this.api + '/cargaacademica/deleteCarga', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
    }

    public getAllPCargaAcademica(): Observable<CargaAcademicaModel[]> {
        return this.http.get<CargaAcademicaModel[]>(this.api + `/cargaacademica/getAllCarga`);
    }
    public getAllPCargaAcademicaXSemestre(semestre): Observable<CargaAcademicaModel[]> {
        return this.http.get<CargaAcademicaModel[]>(this.api + `/cargaacademica/getAllCarga?semestre=${semestre}`);
    }
    public getAllPCargaAcademicaXdocente(id): Observable<CargaAcademicaModel[]> {
        return this.http.get<CargaAcademicaModel[]>(this.api + `/cargaacademica/getCargaByProfesor?idprofesor=${id}`);
    }
    public getAllDocentes(): Observable<DocentesModel[]> {
        return this.http.get<DocentesModel[]>(this.api + `/persona/getAllPersona?persSolicitud=hhh`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // Guardar y editar
    public saveOrUpdateDocente(docente: DocentesModel): Observable<DocentesModel> {
        return this.http.post<DocentesModel>(this.api + `/persona/saveOrUpdateProfesor`, docente, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // Guardar y editar
    public saveOrUpdateAlumno(alumno: PersonalModel): Observable<PersonalModel> {
        return this.http.post<PersonalModel>(this.api + `/persona/saveOrUpdateEstudiante`, alumno, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }

    // Guardar y editar
    public saveOrUpdateCargaAcademica(valor: any): Observable<any> {

        return this.http.post<any>(this.api + `/cargaacademica/saveCarga`, valor, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }

    // ========================== Personal ==============================
    public getAllPersonal(tipo): Observable<CargaAcademicaModel[]> {
        return this.http.get<CargaAcademicaModel[]>(this.api + `/persona/getAllPersona?persSolicitud=${tipo}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // Guardar y editar
    public saveOrUpdatePersonal(empleado: any): Observable<any> {
        return this.http.post<any>(this.api + `/persona/saveOrUpdateEmpleado`, empleado, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // ========================== Respuesta ==============================
    // Guardar y editar
    public saveOrUpdateRespuesta(respuesta: any): Observable<any> {
        return this.http.post<any>(this.api + `/respuesta/loadSrpta`, respuesta, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    public saveOrUpdateRespuestaPersonal(respuesta: any): Observable<any> {
        return this.http.post<any>(this.api + `/respuesta/loadRptaPersonal`, respuesta, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }

    // ========================== Encuestado ==============================
    // Guardar y editar
    public saveOrUpdateEncuestador(alumno: any): Observable<EncuestadorModel> {
        return this.http.post<EncuestadorModel>(this.api + `/respuesta/saveDetalleEncuestado`, JSON.stringify(alumno), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // ========================== verificar ==============================
    public getVerificarEncuesta(idestudiante, idencuesta): Observable<CargaAcademicaModel[]> {
        return this.http.get<CargaAcademicaModel[]>(this.api + `/respuesta/verificarEstadoEncuesta?idestudiante=${idestudiante}&idencuesta=${idencuesta}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // ========================== Reporte ==============================
    // listar con comentarios me manda
    public ReporteDocenteXxSemestre(iddocente, semestre): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/respuesta/getReporteEncuestaByCarga?idcarga=${iddocente}&semestre=${semestre}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    public ReporteRankingdocente(semestre): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/respuesta/getRankingDocentes?semestre=${semestre}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    public ReporteConsolidadoByCarga(idprofesor, semestre = '2020-II'): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/respuesta/getReporteConsolidadoByCarga?idprofesor=${idprofesor}&semestre=${semestre}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    public ReporteConsolidadoByAdministrad(idpersona, semestre = '2020-II'): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/respuesta/getReporteConsolidadoByAdministrativo?idpersonal=${idpersona}&semestre=${semestre}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    public ReporteRankingEncuesta(idencuesta): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/respuesta/getRankingDocentes?idencuesta=${idencuesta}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    public ReporteGeneralEncuesta(): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/respuesta/getReporteGeneralByPregunta?idescuela=27&semestre=2020-II`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    public ReporteEstudiantesxEncuesta(encuesta:any): Observable<any[]> {        
        return this.http.get<any[]>(this.api + `/encuesta/cantidadPorEscuelas?idencuesta=${encuesta}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // bolero falaz - aterciopelados

    // ========================== Asignatura ==============================
    public getAllAsignatura(): Observable<CargaAcademicaModel[]> {
        return this.http.get<CargaAcademicaModel[]>(this.api + `/asignatura/getAllAsignaturas`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // like
    public getAllAsignaturaXLike(tipo): Observable<CargaAcademicaModel[]> {
        return this.http.get<CargaAcademicaModel[]>(this.api + `getAsignaturaByLike/${tipo}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // Guardar y editar
    public saveOrUpdateAsignatura(curso: any): Observable<any> {
        return this.http.post<any>(this.api + `/asignatura/saveOrUpdateAsignatura`, curso, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // ========================== Asignatura ==============================
    public getAllEscuelas(): Observable<CargaAcademicaModel[]> {
        return this.http.get<CargaAcademicaModel[]>(this.api + `/asignatura/getAllEscuela`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    public getCargaAcademicaXtresinfo(prof, sigla, tipo): Observable<CursosListModel[]> {
        return  this.http.get<any>(this.api + `/distribucion/getCargaAcademica?codigoprof=${prof}&sigla=${sigla}&tipo=${tipo}`);
    }

    public deletePregunta(id: number): Observable<any> {
        return  this.http.get<any>(this.api + `/respuesta/eliminarRespuesta?idrespuesta=${id}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }

    
}
