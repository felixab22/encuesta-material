import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  EncuestaService, SimaService,
} from './services.index';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    EncuestaService,
    SimaService,
  ]
})
export class ServicesModule { }
