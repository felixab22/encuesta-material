import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { NgxChartsModule } from '@swimlane/ngx-charts';

// import { ChartsModule } from 'ng2-charts'

import { SharedModule } from './shared/shared.module';
import { PagesModule } from './pages/pages.module';
import { CustomMaterialModule } from './configuration/CustomMaterial.module';
import { ServicesModule } from './service/services.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AuthService } from './core/auth.service';
import { AuthGuard } from './core/auth.guard';
import { TokenStorage } from './core/token.storage';
import { Interceptor } from './core/app.interceptor';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    SharedModule,
    PagesModule,
    HttpClientModule,
    ServicesModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SearchPipeModule,
    // NgxChartsModule
    // ChartsModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: "es" },
    AuthService,
    AuthGuard,
    TokenStorage,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
