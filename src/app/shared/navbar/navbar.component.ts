import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  mostrar = false;
  estudiante: any;
  persona: any;
  nombre= 'prueba'
  constructor(
    private _router: Router
  ) {
    // localStorage.getItem('autorization')

  }

  ngOnInit() {
    if (localStorage.getItem('autorization') === 'ADMIN') {
      this.mostrar = true;
    }
    if(localStorage.getItem('alumno')){
      this.nombre = JSON.parse(localStorage.getItem('alumno')).apellido + ',' +JSON.parse(localStorage.getItem('alumno')).nombre|| 'Hola';
    } else {
      const persona = JSON.parse(localStorage.getItem('persona')) || 'Hola';
      this.nombre = persona.nombre +' '+ persona.apellido
    }
    
  } 
  Salir() {
    localStorage.clear();
    this._router.navigate([''])
  }
}
