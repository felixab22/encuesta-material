import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth.service';
import { LoginModel } from 'src/app/model/encuesta.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  // validatingForm: FormGroup;

  constructor(

    public dialogRef: MatDialogRef<LoginComponent>,
    @Inject(MAT_DIALOG_DATA) public validatingForm: FormGroup
  ) {
    // this.newLogin = new LoginModel();
  }

  ngOnInit() {
    this.validatingForm = new FormGroup({
      username: new FormControl('',   Validators.required),
      password: new FormControl('',  Validators.required),
    });
  }
  get username() {
    return this.validatingForm.get('username');
  }
  get password() {
    return this.validatingForm.get('password');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
