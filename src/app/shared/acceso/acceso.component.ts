import { Component, OnInit } from '@angular/core';
import { AlumnoAuthService } from 'src/app/core/alumno.auth';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth.service';
import { LoginComponent } from '../login/login.component';
import { MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SimaService } from 'src/app/service/sima.service';
import { EncuestaService } from 'src/app/service/services.index';
declare var swal: any;

@Component({
  selector: 'app-acceso',
  templateUrl: './acceso.component.html',
  styles: ['']
})
export class AccesoComponent implements OnInit {
  // codigo: string;
  // dni: string;
  validatingForm: FormGroup;

  constructor(
    private authAlumnoService: AlumnoAuthService,
    private _router: Router,
    private authService: AuthService,
    public dialog: MatDialog,
    private _sima: SimaService,
    private _encuestaSrv: EncuestaService
  ) { }

  ngOnInit() {
    this.validatingForm = new FormGroup({
      codigo: new FormControl('', [Validators.minLength(8), Validators.maxLength(8), Validators.required]),
      dni: new FormControl('', [Validators.minLength(8), Validators.maxLength(8), Validators.required]),
    });
  }


  get codigo() {
    return this.validatingForm.get('codigo');
  }
  get dni() {
    return this.validatingForm.get('dni');
  }
  loginAlumno(form): void {
    this.authAlumnoService.attemptAuthAlumno(form.codigo, form.dni).subscribe((res: any) => {
      if (res.code === 200) {
        localStorage.setItem('alumno', JSON.stringify(res.data));
        this._router.navigate(["/Sistemas/Encuesta/Bienvenido"]);
      } else if (res.code === 204) {
        // this.enviarSima(form.codigo)      
      } else if (res.code === 201) {
        swal('AVISO!', res.message, 'warning');
      }
    }
    );
  }
  guardarAlumno(dato) {
    this._encuestaSrv.saveOrUpdateAlumno(dato).subscribe((res: any) => {
      if (res.code === 200) {
        localStorage.setItem('alumno', JSON.stringify(res.data));
        this._router.navigate(["/Sistemas/Encuesta/Bienvenido"]);
      }
      console.log(res);

    })
  }
  enviarSima(codigo) {
    this._sima.getToken(codigo).subscribe((res: any) => {
      console.log(res);
      // this.guardarAlumno(res)   
    })
  }

  login(username, password): void {
    this.authService.attemptAuth(username, password)
      .subscribe(
        data => {
          localStorage.setItem('persona', JSON.stringify(data.persona));
          localStorage.setItem('usertoken', data.accessToken);
          localStorage.setItem('autorization', data.authorities[0].authority);
          this._router.navigate(["/Sistemas/Encuesta/Bienvenido"]);
        },
        error => {
          alert("Credenciale incorrectos!");
        }
      );
  }


  Administrador() {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '350px',
      height: '385px',
      data: null
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.login(result.username, result.password);
      } else {
        swal('ERROR!', 'ACCESO DENEGADO A ADMINISTRADOR', 'warning');
      }
    });
  }
}

