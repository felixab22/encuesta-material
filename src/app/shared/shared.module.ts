import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CustomMaterialModule } from '../configuration/CustomMaterial.module';
import { FooterComponent } from './footer/footer.component';
import { AccesoComponent } from './acceso/acceso.component';
// import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [LoginComponent, NavbarComponent, FooterComponent, AccesoComponent],
  imports: [
    CommonModule,
    CustomMaterialModule,
    ReactiveFormsModule
    // RouterModule
  ],
  exports: [
    LoginComponent,
    NavbarComponent,
    FooterComponent,
    AccesoComponent
  ],
  bootstrap: [LoginComponent]
})
export class SharedModule { }
