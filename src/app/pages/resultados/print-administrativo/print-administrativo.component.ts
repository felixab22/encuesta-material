import { Component, OnInit } from '@angular/core';
import { ComentarioModel, EncuestaModel, GraficaModel, PersonalModel, QuestionsResponseModel } from 'src/app/model/encuesta.model';
import { EncuestaService } from 'src/app/service/services.index';
import * as XLSX from 'xlsx';
declare var swal:any;
@Component({
  selector: 'app-print-administrativo',
  templateUrl: './print-administrativo.component.html',
  styleUrls: ['./print-administrativo.component.scss']
})
export class PrintAdministrativoComponent implements OnInit {
  mostrarExel = false;
  buscar = '';
  total = 1;
  fileName = 'reporte_Administrativo.xlsx';
  listarAdminis = new Array<PersonalModel>();
  contadidpregunta = 0;
  unadmin = new PersonalModel();
  listcomentarios = new Array<ComentarioModel>();
  encuesta = new EncuestaModel();
  listaPreguntas = new Array<QuestionsResponseModel>();
  insertartotal: boolean;

  graficando: any = [
    {
      descrip: 'PREGUNTAS',
      val1: 'ALTERNATIVAS',
    }
  ]
  constructor(
    private _EncuestaSrv: EncuestaService
  ) { 
    this.encuesta = JSON.parse(localStorage.getItem('graficarde'));

  }

  ngOnInit() {
    this.listarAdministrativos();
    this.listarPreguntas(this.encuesta.idencuesta);

  }  
  listarPreguntas(id) {
    this._EncuestaSrv.getAllPreguntaXidencuesta(id).subscribe((res: any) => {
      this.listaPreguntas = res.data;
      localStorage.setItem('preguntas', JSON.stringify(this.listaPreguntas));
    });
  }
  listarAdministrativos() {
    this._EncuestaSrv.getAllPersonal('PERSONAL').subscribe((res: any) => {
      if(res.code === 200){
        this.listarAdminis = res.data;
      }      
    });
  }
  createPDF() {
    var sTable = document.getElementById("imprimirpfg").innerHTML;
    var style = "<style>";
    style =
      style +
      "div.pageA4 {font-family: Tahoma, Geneva, Verdana, sans-serif; width: 210mm; height: 245mm;}";
    style =
      style +
      ".logo-header { background-image: url('./../../../../assets/img/cabeza-1.png')!important; height: 150px; width: 210mm;}";
    style =
      style +
      "table {border: 1px solid black;font-size:13px;border-collapse: collapse;}";
    style = style + "table tbody tr:nth-child(odd) {font-weight: bold;}";
    style = style + "th {border: 1px solid black;}";
    style = style + "td {border: 1px solid black;}";
    style = style + ".decription {width:40%}";
    style = style + ".alternativa {width:20px; text-align: center;}";
    style =
      style + "td.comentario {text-align: left;font-weight: normal!important;}";
    style = style + ".amarillo { background-color: rgb(241, 201, 66);}";
    style = style + "td.icono {display:none}";
    // style = style + ".decription {border: 1px solid black; width: 210mm; height: 245mm;}";

    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open("", "", "height=700,width=700");
    win.document.write("<html><head>");
    win.document.write("<title>imprimir</title>"); // <title> FOR PDF HEADER.
    win.document.write(style); // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write("</head>");
    win.document.write("<body>");
    win.document.write(sTable); // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write("</body></html>");
    win.document.close(); // CLOSE THE CURRENT WINDOW.
    win.print(); // PRINT THE CONTENTS.
  }

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);

  }
  eliminarcomentario(valor: any) {
    console.log(valor);    
    this._EncuestaSrv
      .deletePregunta(valor.idrespuesta)
      .subscribe((res: any) => {
        if (res.code === 200) {
          this.graficando = this.graficando.filter(
            (u) => valor.idrespuesta !== u.idrespuesta
          );
        }
      });
  }
  AdministrativoChart(valor){
    console.log(valor);
    
    this.fileName = `reporte_Administrativo.xlsx`

    this.graficando = [
      {
        descrip: 'PREGUNTAS',
        val1: 'ALTERNATIVAS',
        large1: 5,
      }
    ];
    this.unadmin = valor;
    this._EncuestaSrv.ReporteConsolidadoByAdministrad(valor.idpersona).subscribe((res:any)=>{
      if(res.code === 200){
        var otro = {
          asignatura: "ANGULAR",
          modalidadcurso: "LABORATORIO",
          sigla: "AB-222",
          idcarga: 0,
          enunciadopregunta: "PREGUNTA 1",
          idpregunta: 64,
          tipoAlternativa: "UNICO",
          calificacion: "CASI NUNCA",
          cantidad: 10
        }
        var enviar = res.data;
        this.listcomentarios = res.comentarios;
        enviar.push(otro);
        this.mostrarExel = true;
        this.insertartotal = true;

        this.convertirlapregunta(enviar);
        this.fileName = `${valor.idpersona}-${this.fileName}`;

      } else {
         swal('Mal!', 'error', 'warning');
      }
      this.agregarComentarios();
    });
    
  }
  agregarComentarios(){
    for (let i = 0; i < this.listcomentarios.length; i++) {
      this.graficando.push({ descrip: 'COMENTARIO' ,val1: this.listcomentarios[i].comentario,large1: 5, idrespuesta: this.listcomentarios[i].idrespuesta, });
    }
    
  }
  convertirlapregunta(lista: GraficaModel[]) {
    var contador = 0;
    var cortardonde = 0;
    do {
      if (this.contadidpregunta !== lista[contador].idpregunta) {
        if (this.contadidpregunta !== 0) {
          var arreglocortado = lista.slice(0, cortardonde);
          if (arreglocortado.length === 1 && arreglocortado[0].calificacion !== 'RANGO' && arreglocortado[0].calificacion !== 'COMENTARIO') {
            this.listarparauno(arreglocortado)
          }
          if (arreglocortado.length === 2 && arreglocortado[0].calificacion !== 'RANGO' && arreglocortado[0].calificacion !== 'COMENTARIO') {
            this.listarparados(arreglocortado)
          }
          if (arreglocortado.length === 3 && arreglocortado[0].calificacion !== 'RANGO' && arreglocortado[0].calificacion !== 'COMENTARIO') {
            this.listarparatres(arreglocortado)
          }
          if (arreglocortado.length === 4 && arreglocortado[0].calificacion !== 'RANGO' && arreglocortado[0].calificacion !== 'COMENTARIO') {
            this.listarparacuatro(arreglocortado)
          }
          if (arreglocortado.length === 5 && arreglocortado[0].calificacion !== 'RANGO' && arreglocortado[0].calificacion !== 'COMENTARIO') {
            this.listarparacinco(arreglocortado)
          }
         lista.splice(0, cortardonde);
          contador = 0;
          cortardonde = 0;
        }
        this.contadidpregunta = lista[contador].idpregunta

      } else {
        cortardonde++;
        contador++;
      }
    } while (lista.length > 1)

  }
  buscarpregunta(id) {
    var volver = this.listaPreguntas.find(lista => lista.pregunta.idpregunta === id);
    var listalter = volver.pregunta.alternativaList
    var seleccionados = new Array;
    for (let i = 0; i < listalter.length; i++) {
      seleccionados.push(listalter[i].descripcion);
    }
    return seleccionados;
  }

  listarparauno(lista: GraficaModel[]) {
    this.total = lista[0].cantidad;
    if (this.insertartotal) {
      this.graficando.push(
        {
          descrip: `CARGA ACADEMICA: ${lista[0].asignatura} - ${lista[0].modalidadcurso}`,
          val1: "TOTAL DE ALUMNOS",
          val2: this.total.toString(),
          large1: 4,
          large2: 1,
        },
        { descrip: "PREGUNTAS", val1: "ALTERNATIVAS", large1: 5 }
      );
      this.insertartotal = false;
    }
    var retornado = this.buscarpregunta(lista[0].idpregunta);
    var position = retornado.indexOf(lista[0].calificacion);
    if (retornado.length === 2) {
      var subirdes = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        large1: 2,
        large2: 3,
      };
      var subircantida = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        large1: 2,
        large2: 3,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      if (position === 0) {
        subircantida = {
          descrip: "RESULTADOS",
          val1: a.toString().slice(0, 4) + "%",
          val2: "0%",
          large1: 2,
          large2: 3,
        };
      } else {
        subircantida = {
          descrip: "RESULTADOS",
          val1: "0%",
          val2: a.toString().slice(0, 4) + "%",
          large1: 2,
          large2: 3,
        };
      }
      this.graficando.push(subirdes);
      this.graficando.push(subircantida);
    } else if (retornado.length === 3) {
      var subirdes3 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        large1: 2,
        large2: 2,
        large3: 1,
      };
      var subircantida3 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        large1: 2,
        large2: 2,
        large3: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      switch (position) {
        case 0:
          subircantida3.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = a.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes3);
      this.graficando.push(subircantida3);
    } else if (retornado.length === 4) {
      var subirdes4 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        val4: retornado[3],
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      var subircantida4 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        val4: "0%",
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      switch (position) {
        case 0:
          subircantida4.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = a.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = a.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes4);
      this.graficando.push(subircantida4);
    } else if (retornado.length === 5) {
      var subirdes5 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        val4: retornado[3],
        val5: retornado[4],
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      var subircantida5 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        val4: "0%",
        val5: "0%",
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      switch (position) {
        case 0:
          subircantida5.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida5.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida5.val3 = a.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida5.val4 = a.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida5.val5 = a.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes5);
      this.graficando.push(subircantida5);
    } else {
      var a = (lista[0].cantidad * 100) / this.total;
      var subirdes8 = {
        descrip: lista[0].enunciadopregunta,
        val1: lista[0].calificacion,
        large1: 5,
      };
      var subircantida8 = {
        descrip: "RESULTADOS",
        val1: a.toString() + "%",
        large1: 5,
      };
      this.graficando.push(subirdes8);
      this.graficando.push(subircantida8);
    }
  }

  listarparados(lista: GraficaModel[]) {
    this.total = lista[0].cantidad + lista[1].cantidad;
    if (this.insertartotal) {
      this.graficando.push(
        {
          descrip: `CARGA ACADEMICA: ${lista[0].asignatura} - ${lista[0].modalidadcurso}`,
          val1: "TOTAL DE ALUMNOS",
          val2: this.total.toString(),
          large1: 4,
          large2: 1,
        },
        { descrip: "PREGUNTAS", val1: "ALTERNATIVAS", large1: 5 }
      );
      this.insertartotal = false;
    }
    var retornado = this.buscarpregunta(lista[0].idpregunta);
    var position1 = retornado.indexOf(lista[0].calificacion);
    var position2 = retornado.indexOf(lista[1].calificacion);
    if (retornado.length === 2) {
      var subirdes = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        large1: 2,
        large2: 3,
      };
      var subircantida = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        large1: 2,
        large2: 3,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      if (position1 === 0) {
        subircantida = {
          descrip: "RESULTADOS",
          val1: a.toString().slice(0, 4) + "%",
          val2: b.toString().slice(0, 4) + "%",
          large1: 2,
          large2: 3,
        };
      } else {
        subircantida = {
          descrip: "RESULTADOS",
          val1: b.toString().slice(0, 4) + "%",
          val2: a.toString().slice(0, 4) + "%",
          large1: 2,
          large2: 3,
        };
      }
      this.graficando.push(subirdes);
      this.graficando.push(subircantida);
    } else if (retornado.length === 3) {
      var subirdes3 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        large1: 2,
        large2: 2,
        large3: 1,
      };
      var subircantida3 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        large1: 2,
        large2: 2,
        large3: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida3.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida3.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = b.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes3);
      this.graficando.push(subircantida3);
    } else if (retornado.length === 4) {
      var subirdes4 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        val4: retornado[3],
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      var subircantida4 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        val4: "0%",
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida4.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = a.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida4.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = b.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = b.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes4);
      this.graficando.push(subircantida4);
    } else if (retornado.length === 5) {
      var subirdes5 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        val4: retornado[3],
        val5: retornado[4],
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      var subircantida5 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        val4: "0%",
        val5: "0%",
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida5.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida5.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida5.val3 = a.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida5.val4 = a.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida5.val5 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida5.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida5.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida5.val3 = b.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida5.val4 = b.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida5.val5 = b.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes5);
      this.graficando.push(subircantida5);
    } else {
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var subirdes8 = {
        descrip: lista[0].enunciadopregunta,
        val1: lista[0].calificacion,
        val2: lista[1].calificacion,
        large1: 2,
        large2: 3,
      };
      var subircantida8 = {
        descrip: "RESULTADOS",
        val1: b.toString() + "%",
        val2: a.toString() + "%",
        large1: 2,
        large2: 3,
      };
      this.graficando.push(subirdes8);
      this.graficando.push(subircantida8);
    }
  }

  listarparatres(lista: GraficaModel[]) {
    this.total = lista[0].cantidad + lista[1].cantidad + lista[2].cantidad;
    if (this.insertartotal) {
      this.graficando.push(
        {
          descrip: `CARGA ACADEMICA: ${lista[0].asignatura} - ${lista[0].modalidadcurso}`,
          val1: "TOTAL DE ALUMNOS",
          val2: this.total.toString(),
          large1: 4,
          large2: 1,
        },
        { descrip: "PREGUNTAS", val1: "ALTERNATIVAS", large1: 5 }
      );
      this.insertartotal = false;
    }
    var retornado = this.buscarpregunta(lista[0].idpregunta);
    var position1 = retornado.indexOf(lista[0].calificacion);
    var position2 = retornado.indexOf(lista[1].calificacion);
    var position3 = retornado.indexOf(lista[2].calificacion);
    if (retornado.length === 3) {
      var subirdes3 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        large1: 2,
        large2: 2,
        large3: 1,
      };
      var subircantida3 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        large1: 2,
        large2: 2,
        large3: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida3.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida3.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = b.toString().slice(0, 4) + "%";
          break;
      }
      switch (position3) {
        case 0:
          subircantida3.val1 = c.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = c.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = c.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes3);
      this.graficando.push(subircantida3);
    } else if (retornado.length === 4) {
      var subirdes4 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        val4: retornado[3],
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      var subircantida4 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        val4: "0%",
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida4.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = a.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida4.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = b.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = b.toString().slice(0, 4) + "%";
          break;
      }
      switch (position3) {
        case 0:
          subircantida4.val1 = c.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = c.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = c.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = c.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes4);
      this.graficando.push(subircantida4);
    } else if (retornado.length === 5) {
      var subirdes8 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        val4: retornado[3],
        val5: retornado[4],
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      var subircantida8 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        val4: "0%",
        val5: "0%",
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida8.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida8.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida8.val3 = a.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida8.val4 = a.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida8.val5 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida8.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida8.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida8.val3 = b.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida8.val4 = b.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida8.val5 = b.toString().slice(0, 4) + "%";
          break;
      }
      switch (position3) {
        case 0:
          subircantida8.val1 = c.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida8.val2 = c.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida8.val3 = c.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida8.val4 = c.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida8.val5 = c.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes8);
      this.graficando.push(subircantida8);
    } else {
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      var subirdes9 = {
        descrip: lista[0].enunciadopregunta,
        val1: lista[0].calificacion,
        val2: lista[1].calificacion,
        val3: lista[2].calificacion,
        large1: 2,
        large2: 2,
        large3: 1,
      };
      var subircantida9 = {
        descrip: "RESULTADOS",
        val1: a.toString() + "%",
        val2: b.toString() + "%",
        val3: c.toString() + "%",
        large1: 2,
        large2: 2,
        large3: 1,
      };
      this.graficando.push(subirdes9);
      this.graficando.push(subircantida9);
    }
  }
  listarparacuatro(lista: GraficaModel[]) {
    this.total =
      lista[0].cantidad +
      lista[1].cantidad +
      lista[2].cantidad +
      lista[3].cantidad;
    if (this.insertartotal) {
      this.graficando.push(
        {
          descrip: `CARGA ACADEMICA: ${lista[0].asignatura} - ${lista[0].modalidadcurso}`,
          val1: "TOTAL DE ALUMNOS",
          val2: this.total.toString(),
          large1: 4,
          large2: 1,
        },
        { descrip: "PREGUNTAS", val1: "ALTERNATIVAS", large1: 5 }
      );
      this.insertartotal = false;
    }
    var retornado = this.buscarpregunta(lista[0].idpregunta);
    var position1 = retornado.indexOf(lista[0].calificacion);
    var position2 = retornado.indexOf(lista[1].calificacion);
    var position3 = retornado.indexOf(lista[2].calificacion);
    var position4 = retornado.indexOf(lista[3].calificacion);
    if (retornado.length === 3) {
      var subirdes3 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        large1: 2,
        large2: 2,
        large3: 1,
      };
      var subircantida3 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        large1: 2,
        large2: 2,
        large3: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      var d = (lista[3].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida3.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida3.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = b.toString().slice(0, 4) + "%";
          break;
      }
      switch (position3) {
        case 0:
          subircantida3.val1 = c.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = c.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = c.toString().slice(0, 4) + "%";
          break;
      }
      switch (position4) {
        case 0:
          subircantida3.val1 = d.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = d.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = d.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes3);
      this.graficando.push(subircantida4);
    } else if (retornado.length === 4) {
      var subirdes4 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        val4: retornado[3],
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      var subircantida4 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        val4: "0%",
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      var d = (lista[3].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida4.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = a.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida4.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = b.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = b.toString().slice(0, 4) + "%";
          break;
      }
      switch (position3) {
        case 0:
          subircantida4.val1 = c.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = c.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = c.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = c.toString().slice(0, 4) + "%";
          break;
      }
      switch (position4) {
        case 0:
          subircantida4.val1 = d.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = d.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = d.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = d.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes4);
      this.graficando.push(subircantida4);
    } else if (retornado.length === 5) {
      var subirdes5 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        val4: retornado[3],
        val5: retornado[4],
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      var subircantida5 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        val4: "0%",
        val5: "0%",
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      var d = (lista[3].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida5.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida5.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida5.val3 = a.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida5.val4 = a.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida5.val5 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida5.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida5.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida5.val3 = b.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida5.val4 = b.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida5.val5 = b.toString().slice(0, 4) + "%";
          break;
      }
      switch (position3) {
        case 0:
          subircantida5.val1 = c.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida5.val2 = c.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida5.val3 = c.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida5.val4 = c.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida5.val5 = c.toString().slice(0, 4) + "%";
          break;
      }
      switch (position4) {
        case 0:
          subircantida5.val1 = d.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida5.val2 = d.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida5.val3 = d.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida5.val4 = d.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida5.val5 = d.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes5);
      this.graficando.push(subircantida5);
    } else {
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      var d = (lista[3].cantidad * 100) / this.total;
      var subirdes8 = {
        descrip: lista[0].enunciadopregunta,
        val1: lista[0].calificacion,
        val2: lista[1].calificacion,
        val3: lista[2].calificacion,
        val4: lista[3].calificacion,
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      var subircantida8 = {
        descrip: "RESULTADOS",
        val1: a.toString() + "%",
        val2: b.toString() + "%",
        val3: c.toString() + "%",
        val4: d.toString() + "%",
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      this.graficando.push(subirdes8);
      this.graficando.push(subircantida8);
    }
  }
  listarparacinco(lista: GraficaModel[]) {
    this.total =
      lista[0].cantidad +
      lista[1].cantidad +
      lista[2].cantidad +
      lista[3].cantidad +
      lista[4].cantidad;
    if (this.insertartotal) {
      this.graficando.push(
        {
          descrip: `CARGA ACADEMICA: ${lista[0].asignatura} - ${lista[0].modalidadcurso}`,
          val1: "TOTAL DE ALUMNOS",
          val2: this.total.toString(),
          large1: 4,
          large2: 1,
        },
        { descrip: "PREGUNTAS", val1: "ALTERNATIVAS", large1: 5 }
      );
      this.insertartotal = false;
    }
    var retornado = this.buscarpregunta(lista[0].idpregunta);
    var position1 = retornado.indexOf(lista[0].calificacion);
    var position2 = retornado.indexOf(lista[1].calificacion);
    var position3 = retornado.indexOf(lista[2].calificacion);
    var position4 = retornado.indexOf(lista[3].calificacion);
    var position5 = retornado.indexOf(lista[4].calificacion);
    if (retornado.length === 3) {
      var subirdes3 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        large1: 2,
        large2: 2,
        large3: 1,
      };
      var subircantida3 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        large1: 2,
        large2: 2,
        large3: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      var d = (lista[3].cantidad * 100) / this.total;
      var e = (lista[4].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida3.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida3.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = b.toString().slice(0, 4) + "%";
          break;
      }
      switch (position3) {
        case 0:
          subircantida3.val1 = c.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = c.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = c.toString().slice(0, 4) + "%";
          break;
      }
      switch (position4) {
        case 0:
          subircantida3.val1 = d.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = d.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = d.toString().slice(0, 4) + "%";
          break;
      }
      switch (position5) {
        case 0:
          subircantida3.val1 = e.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida3.val2 = e.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida3.val3 = e.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes3);
      this.graficando.push(subircantida3);
    } else if (retornado.length === 4) {
      var subirdes4 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        val4: retornado[3],
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      var subircantida4 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        val4: "0%",
        large1: 2,
        large2: 1,
        large3: 1,
        large4: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      var d = (lista[3].cantidad * 100) / this.total;
      var e = (lista[4].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida4.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = a.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida4.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = b.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = b.toString().slice(0, 4) + "%";
          break;
      }
      switch (position3) {
        case 0:
          subircantida4.val1 = c.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = c.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = c.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = c.toString().slice(0, 4) + "%";
          break;
      }
      switch (position4) {
        case 0:
          subircantida4.val1 = d.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = d.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = d.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = d.toString().slice(0, 4) + "%";
          break;
      }
      switch (position5) {
        case 0:
          subircantida4.val1 = e.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida4.val2 = e.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida4.val3 = e.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida4.val4 = e.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes4);
      this.graficando.push(subircantida4);
    } else if (retornado.length === 5) {
      var subirdes2 = {
        descrip: lista[0].enunciadopregunta,
        val1: retornado[0],
        val2: retornado[1],
        val3: retornado[2],
        val4: retornado[3],
        val5: retornado[4],
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      var subircantida2 = {
        descrip: "RESULTADOS",
        val1: "0%",
        val2: "0%",
        val3: "0%",
        val4: "0%",
        val5: "0%",
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      var d = (lista[3].cantidad * 100) / this.total;
      var e = (lista[4].cantidad * 100) / this.total;
      switch (position1) {
        case 0:
          subircantida2.val1 = a.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida2.val2 = a.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida2.val3 = a.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida2.val4 = a.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida2.val5 = a.toString().slice(0, 4) + "%";
          break;
      }
      switch (position2) {
        case 0:
          subircantida2.val1 = b.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida2.val2 = b.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida2.val3 = b.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida2.val4 = b.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida2.val5 = b.toString().slice(0, 4) + "%";
          break;
      }
      switch (position3) {
        case 0:
          subircantida2.val1 = c.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida2.val2 = c.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida2.val3 = c.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida2.val4 = c.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida2.val5 = c.toString().slice(0, 4) + "%";
          break;
      }
      switch (position4) {
        case 0:
          subircantida2.val1 = d.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida2.val2 = d.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida2.val3 = d.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida2.val4 = d.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida2.val5 = d.toString().slice(0, 4) + "%";
          break;
      }
      switch (position5) {
        case 0:
          subircantida2.val1 = e.toString().slice(0, 4) + "%";
          break;
        case 1:
          subircantida2.val2 = e.toString().slice(0, 4) + "%";
          break;
        case 2:
          subircantida2.val3 = e.toString().slice(0, 4) + "%";
          break;
        case 3:
          subircantida2.val4 = e.toString().slice(0, 4) + "%";
          break;
        case 4:
          subircantida2.val5 = e.toString().slice(0, 4) + "%";
          break;
      }
      this.graficando.push(subirdes2);
      this.graficando.push(subircantida2);
    } else {
      var a = (lista[0].cantidad * 100) / this.total;
      var b = (lista[1].cantidad * 100) / this.total;
      var c = (lista[2].cantidad * 100) / this.total;
      var d = (lista[3].cantidad * 100) / this.total;
      var e = (lista[4].cantidad * 100) / this.total;
      var subirdes = {
        descrip: lista[0].enunciadopregunta,
        val1: lista[0].calificacion,
        val2: lista[1].calificacion,
        val3: lista[2].calificacion,
        val4: lista[3].calificacion,
        val5: lista[4].calificacion,
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      var subircantida = {
        descrip: "RESULTADOS",
        val1: a.toString() + "%",
        val2: b.toString() + "%",
        val3: c.toString() + "%",
        val4: d.toString() + "%",
        val5: e.toString() + "%",
        large1: 1,
        large2: 1,
        large3: 1,
        large4: 1,
        large5: 1,
      };
      this.graficando.push(subirdes);
      this.graficando.push(subircantida);
    }
  }
  // listarparaseis(lista: GraficaModel[]) {
  //   this.total = lista[0].cantidad + lista[1].cantidad + lista[2].cantidad + lista[3].cantidad + lista[4].cantidad + lista[5].cantidad;
  //   var subirdes = { descrip: lista[0].enunciadopregunta, val1: lista[0].calificacion, val2: lista[1].calificacion, val3: lista[2].calificacion, val4: lista[3].calificacion, val5: lista[4].calificacion, val6: lista[5].calificacion };
  //   var subircantida = { descrip: 'CANTIDAD', val1: lista[0].cantidad * 100 / this.total, val2: lista[1].cantidad * 100 / this.total, val3: lista[2].cantidad * 100 / this.total, val4: lista[3].cantidad * 100 / this.total, val5: lista[4].cantidad * 100 / this.total, val6: lista[5].cantidad * 100 / this.total };
  //   this.graficando.push(subirdes);
  //   this.graficando.push(subircantida);
  // }
  // listarparasiete(lista: GraficaModel[]) {
  //   this.total = lista[0].cantidad + lista[1].cantidad + lista[2].cantidad + lista[3].cantidad + lista[4].cantidad + lista[5].cantidad + lista[6].cantidad;
  //   var subirdes = { descrip: lista[0].enunciadopregunta, val1: lista[0].calificacion, val2: lista[1].calificacion, val3: lista[2].calificacion, val4: lista[3].calificacion, val5: lista[4].calificacion, val6: lista[5].calificacion, val7: lista[6].calificacion };
  //   var subircantida = { descrip: 'CANTIDAD', val1: lista[0].cantidad * 100 / this.total, val2: lista[1].cantidad * 100 / this.total, val3: lista[2].cantidad * 100 / this.total, val4: lista[3].cantidad * 100 / this.total, val5: lista[4].cantidad * 100 / this.total, val6: lista[5].cantidad * 100 / this.total, val7: lista[6].cantidad * 100 / this.total };
  //   this.graficando.push(subirdes);
  //   this.graficando.push(subircantida);
  // }

}
