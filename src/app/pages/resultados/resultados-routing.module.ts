import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImprimirComponent } from './imprimir/imprimir.component';
import { PrintAdministrativoComponent } from './print-administrativo/print-administrativo.component';


const routes: Routes = [
  {
    path: 'Docente',
    component: ImprimirComponent
  },
  {
    path: 'Administrativo',
    component: PrintAdministrativoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultadosRoutingModule { }

