import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImprimirComponent } from './imprimir/imprimir.component';
import { ChartsModule } from 'ng2-charts';
import { CustomMaterialModule } from 'src/app/configuration/CustomMaterial.module';
import { ServicesModule } from 'src/app/service/services.module';
import { ResultadosRoutingModule } from './resultados-routing.module';
import { PrintAdministrativoComponent } from './print-administrativo/print-administrativo.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxChartsModule } from '@swimlane/ngx-charts';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [ImprimirComponent, PrintAdministrativoComponent],
  imports: [
    CommonModule,
    ResultadosRoutingModule,
    ChartsModule,
    NgxChartsModule,
    CustomMaterialModule,
    ServicesModule,
    Ng2SearchPipeModule,
    // BrowserAnimationsModule
  ]
})
export class ResultadosModule { }
