import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlternativaModel, EncuestadorModel, QuestionsResponseModel } from 'src/app/model/encuesta.model';
import { EncuestaService } from 'src/app/service/services.index';
declare var swal: any;

@Component({
  selector: 'app-operar-personal',
  templateUrl: './operar-personal.component.html',
  styleUrls: ['./operar-personal.component.scss']
})
export class OperarPersonalComponent implements OnInit {
  newRespuestas = [{ idencuestapregunta: 0, idpersonal: 0, otrarespuesta: '', descripcion: '',ponderacion:0 }];
  unaRespuesta   = { idencuestapregunta: 0, idpersonal: 0, otrarespuesta: '', descripcion: '' ,ponderacion:0};
  resComentario   = { idencuestapregunta: 0, idpersonal: 0, otrarespuesta: '', descripcion: '',ponderacion:0 };
  listaCargaAcademica: any[];
  idencuesta: number;
  listaPreguntas = new Array<QuestionsResponseModel>();
  newEncuestador: EncuestadorModel;
  alumno: any;
  mostrarBoton = false;
  contar = 0;
  autoTicks = false;
  showTicks = false;
  tickInterval = 1;
  vertical = false;
  value = 0;
  comentario='';
  entroComentario = false;
  listageneral: any;
  constructor(
    private _EncuestaSrv: EncuestaService,
    private _router: Router
  ) {
    this.newEncuestador = new EncuestadorModel();
    this.alumno = JSON.parse(localStorage.getItem('alumno'));
    this.idencuesta = JSON.parse(localStorage.getItem('id')).idencuesta;
    this.listaCargaAcademica = JSON.parse(localStorage.getItem('encuestados'));
   }

  ngOnInit() {
    
    this.listarPreguntas();
  }
  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    localStorage.removeItem('id');
    localStorage.removeItem('encuestados');
    localStorage.removeItem('alumno');
    // localStorage.removeItem('cargasacademicas');
  }
  listarPreguntas() {
    this._EncuestaSrv.getAllPreguntaXidencuesta(this.idencuesta).subscribe((res: any) => { 
      this.listaPreguntas = res.data;
      this.listageneral = res.data;
      // localStorage.setItem('preguntas',JSON.stringify(this.listaPreguntas));      
    });
  }
  onSelectionChangeAlternativa(alternativa:AlternativaModel, pregunta:QuestionsResponseModel) {   
    this.contar++;
    if(this.contar === this.listaPreguntas.length) {
      this.mostrarBoton = true;
    }
    
    const u = this.listaPreguntas.find(u => pregunta.idencuestapregunta === u.idencuestapregunta);
    u.pregunta.estado =true;  
    this.unaRespuesta = { idencuestapregunta: 0, idpersonal: 0, otrarespuesta: '', descripcion:'',ponderacion:0};
    this.unaRespuesta.idencuestapregunta = pregunta.idencuestapregunta;
    this.unaRespuesta.idpersonal = this.listaCargaAcademica[0].idpersonal; // cambiar cuando traiga a los profesores
    this.unaRespuesta.otrarespuesta = '';
    this.unaRespuesta.descripcion = alternativa.descripcion;
    if (this.newRespuestas[0].idencuestapregunta === 0) {
      this.newRespuestas[0] = this.unaRespuesta;
    } else {
      if (this.encontrado(pregunta).verdad === false) {
        this.newRespuestas.push(this.unaRespuesta);
      } else {
        const j = this.encontrado(pregunta).largo
        this.newRespuestas[j] = this.unaRespuesta;
      }
    }
  }
  encontrado(pregunta: any): any {
    var volver = { verdad: false, largo: 0 };
    for (let i = 0; i < this.newRespuestas.length; i++) {
      if (this.newRespuestas[i].idencuestapregunta === pregunta.idencuestapregunta) {
        volver.verdad = true;
        volver.largo = i;
      }
    }
    return volver;
  }
  EnviarEncuesta() {
    if(this.entroComentario===true){
      this.newRespuestas.push(this.resComentario);
    }
    this._EncuestaSrv.saveOrUpdateRespuestaPersonal(this.newRespuestas).subscribe((res: any) => {
      if (res.code === 200) {
         swal('BIEN!', 'ENCUESTA GUARDADO CORRECTAMENTE!', 'success');
        this.listaPreguntas = new Array<QuestionsResponseModel>();
        this.listarPreguntas();
        this.listaPreguntas = this.listageneral;
        this.contar = 0;
        this.mostrarBoton = false;
        this.comentario='';
        this.entroComentario=false;
        this.listaCargaAcademica.shift();
        if (this.listaCargaAcademica.length > 1) {         
          this.newRespuestas = [{ idencuestapregunta: 0, idpersonal: 0, otrarespuesta: '', descripcion: '',ponderacion:0 }];
          this.unaRespuesta = { idencuestapregunta: 0, idpersonal: 0, otrarespuesta: '', descripcion: '',ponderacion:0 };
          this.value=0;
          for (let i = 0; i < this.listaPreguntas.length; i++) {
              Array.from(document.querySelectorAll(`[name=pregunta${i}]`)).forEach((x: any) => x.checked = false);
            
          }          
          if(this.listaPreguntas.length > 1) {
            // localStorage.setItem('cargasacademicas',JSON.stringify(this.listaCargaAcademica));
          }
        } else  if(this.listaCargaAcademica.length === 1){
          this.GuardarEncuestador();
        }
      } else {
        alert('error fatal');
      }
    }    
    );
    
  }
  somethingChanged(pregunta){
    if(this.comentario.length > 3){
      this.entroComentario = true;
      if(this.comentario.length === 4) {
        this.contar++;
        const u = this.listaPreguntas.find(u => pregunta.idencuestapregunta === u.idencuestapregunta);
        u.pregunta.estado =true;
      }     
       if(this.contar >= this.listaPreguntas.length) {
        this.mostrarBoton = true;
      }
      this.resComentario = { idencuestapregunta: pregunta.idencuestapregunta, idpersonal: this.listaCargaAcademica[0].idpersonal, otrarespuesta: this.comentario, descripcion: 'COMENTARIO',ponderacion:0 };
    } else{
      return
    }
  }
  GuardarEncuestador() {
    this.newEncuestador.idestudiante = this.alumno.idpersona;
    this.newEncuestador.idencuesta = this.idencuesta;
    this.newEncuestador.fencuesta = new Date;
    
    this._EncuestaSrv.saveOrUpdateEncuestador(this.newEncuestador).subscribe((res: any) => {
      if(res.code === 200){        
        swal('BIEN!', 'ENCUESTA FINALIZADO!', 'success');
        this.listaPreguntas = null;
        localStorage.setItem('alumno', JSON.stringify(this.alumno));
        this._router.navigate(['/Sistemas/Encuesta/Bienvenido']);
      }      
    });
  }
  cambios(valor, pregunta){
    this.contar++;
    if(this.contar >= this.listaPreguntas.length) {
      this.mostrarBoton = true;
    }
    
    const u = this.listaPreguntas.find(u => pregunta.idencuestapregunta === u.idencuestapregunta);
    u.pregunta.estado =true;  
    this.unaRespuesta = { idencuestapregunta: 0, idpersonal: 0, otrarespuesta: '', descripcion:'',ponderacion:0};
    this.unaRespuesta.idencuestapregunta = pregunta.idencuestapregunta;
    this.unaRespuesta.idpersonal = this.listaCargaAcademica[0].idpersonal; // cambiar cuando traiga a los profesores
    this.unaRespuesta.ponderacion = this.value;
    this.unaRespuesta.descripcion = 'RANGO';
    if (this.newRespuestas[0].idencuestapregunta === 0) {
      this.newRespuestas[0] = this.unaRespuesta;
    } else {
      if (this.encontrado(pregunta).verdad === false) {
        this.newRespuestas.push(this.unaRespuesta);
      } else {
        const j = this.encontrado(pregunta).largo
        this.newRespuestas[j] = this.unaRespuesta;
      }
    }
  }
}
