import { Component, OnInit } from '@angular/core';
import { EncuestaService } from 'src/app/service/services.index';
import { CursosListModel, EncuestaModel } from '../../../model/encuesta.model';
import { Router } from '@angular/router';
declare var swal: any;
@Component({
  selector: 'app-bienvenido',
  templateUrl: './bienvenido.component.html',
  styleUrls: ['./bienvenido.component.scss']
})
export class BienvenidoComponent implements OnInit {
  Allencuesta = new Array<EncuestaModel>();
  alumno: any;
  constructor(
    private _EncuestaSrv: EncuestaService,
    private _router: Router
  ) {

  }

  ngOnInit() {
    this.alumno = JSON.parse(localStorage.getItem('alumno'));    
    this.listarEncuesta();
  }
  ngAfterContentInit(): void {
    localStorage.removeItem('alumno');
    
  }
  listarEncuesta() {
    this._EncuestaSrv.getAllEncuesta().subscribe((res: any) => {
      if (res.code === 200) {
        this.Allencuesta = res.data;
      } else {
      }
    })
  }
  irAencuestar(item) {
    if (this.alumno === null) {
      swal('ERROR!', 'ACCEDER CON CUENTA DE ALUMNO', 'warning');
      return
    }
    this._EncuestaSrv.getVerificarEncuesta(this.alumno.idpersona, item.idencuesta).subscribe((res: any) => {
      if (res.code === 200) {
        localStorage.setItem('id', JSON.stringify(item));
        localStorage.setItem('alumno', JSON.stringify(this.alumno));
        localStorage.setItem('encuestados', JSON.stringify(res.data));
        if (item.tipoencuesta === 'DOCENTE') {
          this._router.navigate(['/Sistemas/Encuesta/Academica']);
        } else if (item.tipoencuesta === 'ADMINISTRATIVO') {
          swal({
            title: "ATENCIÓN",
            text: "REALIZAR SOLO SI INTERACTUÓ CON EL PERSONAL ADMINISTRATIVO",
            icon: "warning",
            buttons: ["CANCELAR", 'CONTINUAR'],
            dangerMode: true,
          })
            .then((willDelete) => {
              if (willDelete) {
                swal("DIRECCIONANDO...", {
                  icon: "success",
                }
                );
                this.convertirAdministrativo(res.data);
                this._router.navigate(['/Sistemas/Encuesta/Personal']);
              } else {
                swal("CANCELADO");
              }
            });
        }
      } else if (res.code === 403) {
        swal('AVISO!', 'ENCUESTA YA REALIZADA', 'warning');
      }
    })
  }

  // convertirDocente(lista) {

  //   for (let item of lista) {
  //      this._EncuestaSrv.getCargaAcademicaXtresinfo(item.coddocente,item.sigla, item.modalidad).subscribe((res:any) => {
  //         if(res.code === 200) {
  //           item.idcarga = res.data;
  //         } else {
  //           item.idcarga = 0;
  //         }
  //      })

       
  //   }
    
  //   // localStorage.setItem('encuestados', JSON.stringify(this.todo));

  // }
  convertirAdministrativo(lista) {
    var todo = [{ nombre: 'FELIX', apellido: 'ASTO', idpersonal: 0, cargo: 'SISTEMAS', completed: false, color: 'primary' }];
    for (let i = 0; i < lista.length; i++) {
      var uno = { nombre: '', apellido: '', idpersonal: 0, cargo: '', completed: false, color: 'primary' };
      uno.nombre = lista[i].nombre;
      uno.apellido = lista[i].apellido;
      uno.cargo = lista[i].cargo;
      uno.idpersonal = lista[i].idpersona;
      todo.unshift(uno);
    }
    localStorage.setItem('encuestados', JSON.stringify(todo));
  }

}
