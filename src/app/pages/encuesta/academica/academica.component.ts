import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
declare var swal: any;
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { CursosListModel } from 'src/app/model/encuesta.model';
@Component({
  selector: 'app-academica',
  templateUrl: './academica.component.html',
  styleUrls: ['./academica.component.scss']
})
export class AcademicaComponent implements OnInit {
  
  todo = new Array<CursosListModel>();
  Resultado: any;
  tipo = 'DOCENTE';
  curso = 'CURSO';
  titulo = 'CARGA ACADEMICA'
  cargando = false;
  dataSource = new MatTableDataSource<any>(this.todo);
  
  displayedColumns: string[] = ['docente', 'curso', 'sigla', 'modalidad'];
  encuesta: any;
  alumno: any;
  constructor(
    private _router: Router,
    ) {
      this.encuesta = JSON.parse(localStorage.getItem('id'));
      this.alumno = JSON.parse(localStorage.getItem('alumno'));
      this.todo = JSON.parse(localStorage.getItem('encuestados'));
      this.dataSource = new MatTableDataSource<any>(this.todo);
      this.cargando = true;
  }

  ngOnInit() {    
    setTimeout(() => {
      if (this.encuesta.tipoencuesta === 'DOCENTE') {
        this.tipo = 'DOCENTE';
        this.curso = 'CURSO';
        this.titulo = 'CARGA ACADEMICA'

      } else {
        this.tipo = 'ADMINISTRATIVO';
        this.curso = 'CARGO';
        this.titulo = 'ADMINISTRATIVO'
      }
    }, 1000);
  }
  ngAfterViewInit() {    
    localStorage.removeItem('id');
    localStorage.removeItem('alumno');
    localStorage.removeItem('encuestados');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

 
  GerenerarEncuesta() {
      var agregar = {coddocente: "410136", curso: "ANGULAR", docente: "FELIX", idcarga: 251, modalidad: "LABORATORIO", sigla: "IS-444"};
      this.todo.push(agregar);
    swal({
      title: "USTED ESTA LISTO PARA ENCUESTAR",
      text: "¿UNA VEZ INGRESADO NO DEBERÁ SALIR HASTA TERMINAR LA ENCUESTA?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          swal("DIRECCIONANDO...", {
            icon: "success",
          }
          );
          localStorage.setItem('alumno', JSON.stringify(this.alumno));

          localStorage.setItem('cargasacademicas', JSON.stringify(this.todo ));
          localStorage.setItem('id',JSON.stringify(this.encuesta));
          this._router.navigate(['/Sistemas/Encuesta/Operar']);
        } else {
          swal("CANCELADO");
        }
      });

  }
  ngOnDestroy(): void {
    localStorage.setItem('alumno', JSON.stringify(this.alumno));  
    localStorage.removeItem('encuestados');
  }
}

