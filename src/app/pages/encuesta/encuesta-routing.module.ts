import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcademicaComponent } from './academica/academica.component';
import { OperarComponent } from './operar/operar.component';
import { BienvenidoComponent } from './bienvenido/bienvenido.component';
import { OperarPersonalComponent } from './operar-personal/operar-personal.component';

const routes: Routes = [
  {
    path: 'Academica',
    component: AcademicaComponent
  },
  {
    path: 'Bienvenido',
    component: BienvenidoComponent
  },
  {
    path: 'Operar',
    component: OperarComponent
  },
  {
    path: 'Personal',
    component: OperarPersonalComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EncuestaRoutingModule { }

