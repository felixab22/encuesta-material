import { Component, OnInit } from '@angular/core';
import { EncuestaService } from 'src/app/service/services.index';
import { AlternativaModel, EncuestadorModel, QuestionsResponseModel } from 'src/app/model/encuesta.model';
import { Router } from '@angular/router';
declare var swal: any;
@Component({
  selector: 'app-operar',
  templateUrl: './operar.component.html',
  styleUrls: ['./operar.component.scss']
})
export class OperarComponent implements OnInit {
  newRespuestas = [{ idencuestapregunta: 0, idcarga: 0,  descripcion: '', otrarespuesta:'',ponderacion:0 }];
  unaRespuesta= { idencuestapregunta: 0, idcarga: 0,  descripcion: '',otrarespuesta:'',ponderacion:0 };
  resComentario   = { idencuestapregunta: 0, idcarga: 0, otrarespuesta: '', descripcion: '',ponderacion:0 };

  listaCargaAcademica: any[];
  encuesta: any;
  listaPreguntas = new Array<QuestionsResponseModel>();
  listasgeneral = new Array<QuestionsResponseModel>();
  newEncuestador: EncuestadorModel;
  alumno: any;
  mostrarBoton = false;
  contar = 0;
  autoTicks = false;
  showTicks = false;
  tickInterval = 1;
  vertical = false;
  value = 0;
  comentario='';
  entroComentario = false;
  alumnoyaguardado = false;

  constructor(
    private _EncuestaSrv: EncuestaService,
    private _router: Router
  ) {
    this.newEncuestador = new EncuestadorModel();
    this.alumno = JSON.parse(localStorage.getItem('alumno'));
    this.encuesta = JSON.parse(localStorage.getItem('id'));
    
  }
  
  ngOnInit() {
    this.listaCargaAcademica = JSON.parse(localStorage.getItem('cargasacademicas'));    
    this.convertirDocente();
    this.listarPreguntas();
  }
  ngAfterContentInit(): void { 
    if(this.listaCargaAcademica === undefined || this.listaCargaAcademica === null){
      this._router.navigate(['/Sistemas/Encuesta/Bienvenido']);
    }    
    localStorage.removeItem('cargasacademicas');
    localStorage.removeItem('id');
    localStorage.removeItem('alumno');
  }
  EnviarEncuesta() {
    if(this.entroComentario===true){
      this.newRespuestas.push(this.resComentario);
    }
    this._EncuestaSrv.saveOrUpdateRespuesta(this.newRespuestas).subscribe((res: any) => {

      if (res.code === 200) {
         swal('BIEN!', 'ENCUESTA GUARDADO CORRECTAMENTE!', 'success');
        this.listaPreguntas = new Array<QuestionsResponseModel>();
        this.listarPreguntas();
        this.listaPreguntas = this.listasgeneral;
        this.contar = 0;
        this.mostrarBoton = false;
        this.comentario='';
        this.entroComentario=false;
        this.listaCargaAcademica.shift();
        this.value=0;
        if (this.listaCargaAcademica.length >= 2) {         
          this.newRespuestas = [{ idencuestapregunta: 0, idcarga: 0,  descripcion: '', otrarespuesta:'',ponderacion:0 }];
          this.unaRespuesta = { idencuestapregunta: 0, idcarga: 0,  descripcion: '', otrarespuesta:'',ponderacion:0 };
          for (let i = 0; i < this.listaPreguntas.length; i++) {
            Array.from(document.querySelectorAll(`[name=pregunta${i}]`)).forEach((x: any) => x.checked = false);
          } 
        } else{
          localStorage.setItem('alumno', JSON.stringify(this.alumno));
          this.GuardarEncuestador();
          this._router.navigate(['/Sistemas/Encuesta/Bienvenido']);
        }
      } else if(res.code === 403){
        alert('enviando respuestas más de la cuenta');
      }
    }    
    );
    
  }
  convertirDocente() {

    for (let item of this.listaCargaAcademica) {
       this._EncuestaSrv.getCargaAcademicaXtresinfo(item.coddocente,item.sigla, item.modalidad).subscribe((res:any) => {
          if(res.code === 200) {
            item.idcarga = res.data;
          } else {
            item.idcarga = 0;
          }
       })

       
    }
    
    // localStorage.setItem('encuestados', JSON.stringify(this.todo));

  }



  GuardarEncuestador() {

    this.newEncuestador.idestudiante = this.alumno.idpersona;
    this.newEncuestador.idencuesta = this.encuesta.idencuesta;
    this.newEncuestador.fencuesta = new Date;
    
    this._EncuestaSrv.saveOrUpdateEncuestador(this.newEncuestador).subscribe((res: any) => {
      if(res.code === 200){
        this.alumnoyaguardado = true;    
        swal('BIEN!', 'ENCUESTA FINALIZADO!', 'success');
        this.listaPreguntas = null;
        localStorage.setItem('alumno', JSON.stringify(this.alumno));
        this._router.navigate(['/Sistemas/Encuesta/Bienvenido']);
      }      
    });
  }
  listarPreguntas() {
    this._EncuestaSrv.getAllPreguntaXidencuesta(this.encuesta.idencuesta).subscribe((res: any) => {
      this.listaPreguntas = res.data;        
    });
  }
  onSelectionChangeTipoFamilia(alternativa:AlternativaModel, pregunta:QuestionsResponseModel) {
    // this.contar++;
    if(this.contar >= this.listaPreguntas.length) {
      this.mostrarBoton = true;
    }    
    const u = this.listaPreguntas.find(u => pregunta.idencuestapregunta === u.idencuestapregunta);
    u.pregunta.estado =true;  
    this.unaRespuesta = { idencuestapregunta: 0, idcarga: 0,  descripcion:'',otrarespuesta:'',ponderacion:0};
    this.unaRespuesta.idencuestapregunta = pregunta.idencuestapregunta;
    this.unaRespuesta.idcarga = this.listaCargaAcademica[0].idcarga; // cambiar cuando traiga a los profesores
    this.unaRespuesta.descripcion = alternativa.descripcion;
    if (this.newRespuestas[0].idencuestapregunta === 0) {
      this.newRespuestas[0] = this.unaRespuesta;
      this.contar++;
    } else {
      if (this.encontrado(pregunta).verdad === false) {
        this.newRespuestas.push(this.unaRespuesta);        
        this.contar++;        
        if(this.contar >= this.listaPreguntas.length) {
          this.mostrarBoton = true;
        }
      } else {
        const j = this.encontrado(pregunta).largo
        this.newRespuestas[j] = this.unaRespuesta;
      }
    }
  }
  encontrado(pregunta: any): any {
    var volver = { verdad: false, largo: 0 };
    for (let i = 0; i < this.newRespuestas.length; i++) {
      if (this.newRespuestas[i].idencuestapregunta === pregunta.idencuestapregunta) {
        volver.verdad = true;
        volver.largo = i;
      }
    }
    return volver;
  }
 
  somethingChanged(pregunta){
    if(this.comentario.length > 3){
      this.entroComentario = true;
      if(this.comentario.length === 4) {
        this.contar++;
        const u = this.listaPreguntas.find(u => pregunta.idencuestapregunta === u.idencuestapregunta);
          u.pregunta.estado =true; 
      }
      if(this.contar >= this.listaPreguntas.length) {
        this.mostrarBoton = true;
      }
      this.resComentario = { idencuestapregunta: pregunta.idencuestapregunta, idcarga: this.listaCargaAcademica[0].idcarga, otrarespuesta: this.comentario, descripcion: 'COMENTARIO',ponderacion:0 };
    } else{
      return
    }

  }

  getSliderTickInterval(): number | 'auto' {
    if (this.showTicks) {
      return this.autoTicks ? 'auto' : this.tickInterval;
    }

    return 0;
  }
  cambios(pregunta){
    this.contar++;
    if(this.contar >= this.listaPreguntas.length) {
      this.mostrarBoton = true;
    }
    
    const u = this.listaPreguntas.find(u => pregunta.idencuestapregunta === u.idencuestapregunta);
    u.pregunta.estado =true;  
    this.unaRespuesta = { idencuestapregunta: 0, idcarga: 0, otrarespuesta: '', descripcion:'', ponderacion:0};
    this.unaRespuesta.idencuestapregunta = pregunta.idencuestapregunta;
    this.unaRespuesta.idcarga = this.listaCargaAcademica[0].idcarga; // cambiar cuando traiga a los profesores
    this.unaRespuesta.ponderacion = this.value;
    this.unaRespuesta.descripcion = 'RANGO';
    if (this.newRespuestas[0].idencuestapregunta === 0) {
      this.newRespuestas[0] = this.unaRespuesta;
    } else {
      if (this.encontrado(pregunta).verdad === false) {
        this.newRespuestas.push(this.unaRespuesta);
      } else {
        const j = this.encontrado(pregunta).largo
        this.newRespuestas[j] = this.unaRespuesta;
      }
    }
  }
  ngOnDestroy(): void {
    localStorage.setItem('alumno', JSON.stringify(this.alumno));  
  }
}

