import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EncuestaRoutingModule } from './encuesta-routing.module';
import { AcademicaComponent } from './academica/academica.component';
import { CustomMaterialModule } from 'src/app/configuration/CustomMaterial.module';
import { ServicesModule } from 'src/app/service/services.module';
import { FormsModule } from '@angular/forms';
import { OperarComponent } from './operar/operar.component';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { BienvenidoComponent } from './bienvenido/bienvenido.component';
import { OperarPersonalComponent } from './operar-personal/operar-personal.component';
import { MantenimientosModule } from '../mantenimientos/mantenimientos.module';
import { DesarrolloModule } from '../desarrollo/desarrollo.module';
import { ResultadosModule } from '../resultados/resultados.module';


@NgModule({
  declarations: [AcademicaComponent, OperarComponent, BienvenidoComponent, OperarPersonalComponent],
  imports: [
    CommonModule,
    EncuestaRoutingModule,
    CustomMaterialModule,
    ServicesModule,
    FormsModule,
    Ng2SearchPipeModule,
    MantenimientosModule,
    DesarrolloModule,
    ResultadosModule
  ]
})
export class EncuestaModule { }
