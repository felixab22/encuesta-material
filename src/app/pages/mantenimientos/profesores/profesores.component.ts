import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EncuestaService } from 'src/app/service/services.index';
import { DocentesModel } from 'src/app/model/encuesta.model';
import { ProfesorFormComponent } from '../profesor-form/profesor-form.component';
import { MatDialog } from '@angular/material/dialog';
declare var swal:any;

@Component({
  selector: 'app-profesores',
  templateUrl: './profesores.component.html',
  styleUrls: ['./profesores.component.scss']
})
export class ProfesoresComponent implements OnInit {
  buscar = ''
  validatingForm: FormGroup;
  docentes = new Array<DocentesModel>();
  cargaAcademics = [{ tipo: '', idasignatura: '', iddocente: 0, semestre: '' }]
  constructor(
    private _EncuestaSrv: EncuestaService,
    public dialog: MatDialog,

  ) { }
  ngOnInit() {
    this.validatingForm = new FormGroup({
      coddocente: new FormControl('',  [Validators.required]),
      nombre: new FormControl('',  [Validators.required]),
      apellido: new FormControl('',  [Validators.required]),
      dni: new FormControl('',  [Validators.required, Validators.maxLength(8)])
    });
      this.listarDocentes();
  }
  saveOrUpdateDocente(valor, entrada='new') {
    this._EncuestaSrv.saveOrUpdateDocente(valor).subscribe((res:any)=> {
      
      if(res.code === 200){
        swal('Bien!', 'Guardado!', 'success');
        if(entrada === 'new'){
          this.docentes.push(res.data);
        } else {
          this.listarDocentes();
        }
          // formDirective.resetForm() //reset del form
        }
    }
    )
  }
  listarDocentes(){
    this._EncuestaSrv.getAllDocentes().subscribe((res:any)=>{
      this.docentes = res.data;
    })
  }
  editarEncuesta(valor = null){
    var entrada = 'edit'
    if(valor === null){
      entrada = 'new'
    }
    const dialogRef = this.dialog.open(ProfesorFormComponent, {
      width: '550px',
      data: {
        contact: valor,
        action : entrada
    }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result !== undefined){      
            
            this.saveOrUpdateDocente(result, entrada);
      
        
      }
    });
  }
  
}
