import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlternativaModel } from '../../../model/encuesta.model';

@Component({
  selector: 'app-questio-alter',
  templateUrl: './questio-alter.component.html',
  styleUrls: ['./questio-alter.component.scss']
})
export class QuestioAlterComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<QuestioAlterComponent>,
    @Inject(MAT_DIALOG_DATA) public alternativa: AlternativaModel,
  ) { }

  ngOnInit(): void {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
