import { Component, OnInit } from '@angular/core';
import { PersonalModel } from 'src/app/model/encuesta.model';
import { EncuestaService } from 'src/app/service/services.index';
import { MatDialog } from '@angular/material/dialog';
import { PersonalFormComponent } from '../personal-form/personal-form.component';
declare var swal: any;
@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss']
})
export class PersonalComponent implements OnInit {
  listarAdminis = new Array<PersonalModel>();
  buscar = '';
  constructor(
    private _EncuestaSrv: EncuestaService,
    public dialog: MatDialog,

  ) {

  }

  ngOnInit() {
    this.listarAdministrativos();
  }
  listarAdministrativos() {
    this._EncuestaSrv.getAllPersonal('PERSONAL').subscribe((res: any) => {
      this.listarAdminis = res.data;
    })
  }
  crearAdministrativo(valor) {
    this._EncuestaSrv.saveOrUpdatePersonal(valor).subscribe((res: any) => {
      if (res.code === 200) {
         swal('Bien!', 'PERSONAL CREADO!', 'success');
        this.listarAdministrativos();
      }


    })
  }
  editarEncuesta(valor = null) {
    var entrada = 'edit'
    if (valor === null) {
      entrada = 'new'
    }
    const dialogRef = this.dialog.open(PersonalFormComponent, {
      width: '550px',
      data: {
        contact: valor,
        action: entrada
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.crearAdministrativo(result);
      }
    });
  }
}
