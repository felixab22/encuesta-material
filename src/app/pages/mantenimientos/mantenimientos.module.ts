import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MantenimientosRoutingModule } from './mantenimientos-routing.module';
import { MantenimientoComponent } from './mantenimiento/mantenimiento.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomMaterialModule } from 'src/app/configuration/CustomMaterial.module';
import { ProfesoresComponent } from './profesores/profesores.component';
import { QuestionComponent } from './question/question.component';
import { AlumnadoComponent } from './alumnado/alumnado.component';
import { CursosComponent } from './cursos/cursos.component';
import { CargaComponent } from './carga/carga.component';
import { FormAsignarComponent } from './form-asignar/form-asignar.component';
import { ProfesorFormComponent } from './profesor-form/profesor-form.component';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { CursoFormComponent } from './curso-form/curso-form.component';
import { QuestioFormComponent } from './questio-form/questio-form.component';
import { PersonalComponent } from './personal/personal.component';
import { PersonalFormComponent } from './personal-form/personal-form.component';
import { AlumnadoFormComponent } from './alumnado-form/alumnado-form.component';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { ServicesModule } from 'src/app/service/services.module';
import { QuestioAlterComponent } from './questio-alter/questio-alter.component';

@NgModule({
  declarations: [
    MantenimientoComponent,
    ProfesoresComponent,
    QuestionComponent,
    AlumnadoComponent,
    CursosComponent,
    CargaComponent,
    FormAsignarComponent,
    ProfesorFormComponent,
    CursoFormComponent,
    QuestioFormComponent,
    PersonalComponent,
    PersonalFormComponent,
    AlumnadoFormComponent,
    QuestioAlterComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CustomMaterialModule,
    MantenimientosRoutingModule,
    Ng2SearchPipeModule,
    AngularFileUploaderModule,
    ServicesModule
  ],
  exports:[
    FormAsignarComponent, 
    ProfesorFormComponent,
    CursoFormComponent,
    QuestioFormComponent,
    PersonalFormComponent,
    AlumnadoFormComponent,
    QuestioAlterComponent
  ],
  bootstrap: [
    FormAsignarComponent, 
    ProfesorFormComponent,
    CursoFormComponent,
    QuestioFormComponent,
    PersonalFormComponent,
    AlumnadoFormComponent,
    QuestioAlterComponent
  ]

})
export class MantenimientosModule { }
