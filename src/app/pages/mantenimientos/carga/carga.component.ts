import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DocentesModel } from 'src/app/model/encuesta.model';
import { EncuestaService } from 'src/app/service/services.index';
import { FormAsignarComponent } from '../form-asignar/form-asignar.component';
declare var swal:any;
@Component({
  selector: 'app-carga',
  templateUrl: './carga.component.html',
  styleUrls: ['./carga.component.scss']
})
export class CargaComponent implements OnInit {
  docentes = new Array<DocentesModel>();
  cargaAcademics = [{ tipo: '', idasignatura: '', iddocente: 0, semestre: '' }]
  listaCarga: any;
  buscar='';
  constructor(
    private _EncuestaSrv: EncuestaService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.listarDocentes();
    this.listarCargaAcademica();

  }
  listarDocentes() {
    this._EncuestaSrv.getAllDocentes().subscribe((res: any) => {
      this.docentes = res.data;
    })
  }
  cargarAsignatura(valor) {
    var entrada = 'edit'
    if (valor === null) {
      entrada = 'new'
    }
    const dialogRef = this.dialog.open(FormAsignarComponent, {
      width: '90&',
      height: '90%',
      data: valor
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {        
        this.guardarYEditar(result);
      } else {
      }
    });
  }
  guardarYEditar(valor: any) {
    this.cargaAcademics = [{ tipo: '', idasignatura: '', iddocente: 0, semestre: '' }]

    for (let i = 0; i < valor.length; i++) {
      if (i === 0) {
        this.cargaAcademics[0].idasignatura = valor[i].idasignatura;
        this.cargaAcademics[0].iddocente = valor[i].iddocente;
        this.cargaAcademics[0].semestre = valor[i].semestre;
        this.cargaAcademics[0].tipo = valor[i].tipo;
      } else {
        var temp = { tipo: '', idasignatura: '', iddocente: 0, semestre: '' };
        temp.idasignatura = valor[i].idasignatura;
        temp.semestre = valor[i].semestre;
        temp.iddocente = valor[i].iddocente;
        temp.tipo = valor[i].tipo;
        this.cargaAcademics.push(temp);
      }

    }    
    this._EncuestaSrv.saveOrUpdateCargaAcademica(this.cargaAcademics).subscribe((res: any) => {
      if (res.code === 200) {
         swal('Bien!', 'Guardado!', 'success');
        this.listarCargaAcademica();
      }
    });
  }

  listarCargaAcademica() {
    this._EncuestaSrv.getAllPCargaAcademica().subscribe((res: any) => {
      if(res.code === 200){
        this.listaCarga = res.data;
      }
    })
    // }
  }

}
