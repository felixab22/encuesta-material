import { Component, OnInit, ViewChild } from '@angular/core';
import { AlumnadoFormComponent } from '../alumnado-form/alumnado-form.component';
import { MatDialog } from '@angular/material/dialog';
declare var swal: any;
import { EncuestaService } from 'src/app/service/services.index';
import { PersonalModel } from 'src/app/model/encuesta.model';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-alumnado',
  templateUrl: './alumnado.component.html',
  styleUrls: ['./alumnado.component.scss']
})
export class AlumnadoComponent implements OnInit {
  newAlumno  = new PersonalModel();
  done = [];
  displayedColumns: string[] = ['alumno', 'dni', 'codigo'];
  dataSource = new MatTableDataSource<any>(this.done);
  // @ViewChild(MatSort, { static: true }) sort: MatSort;
  buscar = ''
  constructor(
    private _EncuestaSrv: EncuestaService,
    public dialog: MatDialog,
  ) { }
  ngOnInit() {
    this.listarAlumnos();
  }
  listarAlumnos() {
    this._EncuestaSrv.getAllPersonal('ESTUDIANTE').subscribe((res: any) => {      
      this.done = res.data;
      this.dataSource = new MatTableDataSource<any>(this.done);

    })
  }
  saveOrUpdateAlumnado(valor, entrada){
    this._EncuestaSrv.saveOrUpdateAlumno(valor).subscribe((res:any)=> {
      if(res.code === 200){
        if(entrada === 'edit') {
          swal('BIEN!!', 'EL ALUMNO SE EDITO CORRECTAMENTE!', 'success');
          this.listarAlumnos();
        } else {
          swal('BIEN!', 'EL ALUMNO SE GUARDO CORRECTAMENTE!', 'success');
          this.listarAlumnos();
        }
      }
      
    })
  }
  editarEstudiante(valor = null) {
    var entrada = 'edit'
    if (valor === null) {
      entrada = 'new'
    }
    const dialogRef = this.dialog.open(AlumnadoFormComponent, {
      width: '550px',
      data: {
        contact: valor,
        action: entrada
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {        
        result = [result]
        this.saveOrUpdateAlumnado(result, entrada);
      }
    });
  }
}
