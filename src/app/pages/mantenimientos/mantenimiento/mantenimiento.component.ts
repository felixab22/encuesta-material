import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mantenimiento',
  templateUrl: './mantenimiento.component.html',
  styleUrls: ['./mantenimiento.component.scss']
})
export class MantenimientoComponent implements OnInit {
  listmanten = [
    {
      id: 1,
      denominacion: 'DOCENTES',
      css: 'DOCENTES',
      buton: 'NUEVO DOCENTE',
      ruta: '/Sistemas/Mantenimientos/Profesores',
      descripcion: 'MANTENIEMITO DE LOS DOCENTES DONDE SE PODRÁ AGRAGAR LOS CURSOS QUE IMPARTE EN LA ESCUELA'
    },
    {
      id: 2,
      denominacion: 'PREGUNTAS',
      css: 'PREGUNTAS',
      buton: 'NUEVO PREGUNTA',
      ruta: '/Sistemas/Mantenimientos/Questions',
      descripcion: 'MANTENIEMITO DE LAS PREGUNTAS'
    },
    {
      id: 3,
      denominacion: 'ESTUDIANTES',
      css: 'ESTUDIANTES',
      buton: 'NUEVO ESTUDIANTE',
      ruta: '/Sistemas/Mantenimientos/Estudiantes',
      descripcion: 'MANTENIEMITO DE LAS ESTUDIANTES DE LA ESCUELA'
    },
    {
      id: 4,
      denominacion: 'CURSOS',
      css: 'CURSOS',
      buton: 'NUEVO CURSO',
      ruta: '/Sistemas/Mantenimientos/Cursos',
      descripcion: 'MANTENIEMITO DE LOS CURSOS QUE SE DICTAN'
    },
    {
      id: 5,
      denominacion: 'CARGA ACADEMICA',
      css: 'ACADEMICA',
      buton: 'NUEVO CARGA',
      ruta: '/Sistemas/Mantenimientos/Carga',
      descripcion: 'MANTENIEMITO DE LAS CARGAS ACADEMICAS DE CADA DOCENTES'
    },
    {
      id: 6,
      denominacion: 'ADMINISTRATIVOS',
      css: 'ADMINISTRATIVOS',
      buton: 'NUEVO PERSONAL',
      ruta: '/Sistemas/Mantenimientos/Administrativo',
      descripcion: 'MANTENIEMITO DE LAS CARGAS ACADEMICAS DE CADA DOCENTES'
    },
  ]
  constructor() {

  }

  ngOnInit() {
  }

}
