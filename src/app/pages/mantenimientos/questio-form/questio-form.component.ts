import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { QuestionsModel } from '../../../model/encuesta.model';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-questio-form',
  templateUrl: './questio-form.component.html',
  styleUrls: ['./questio-form.component.scss']
})
export class QuestioFormComponent implements OnInit {
  newQuestion = new QuestionsModel();
  mostarchick=true;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  bloquear = false;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  alternativas = [];
  alterEditar = [];
  botonname = 'CREAR'
  contact: any;
  dialogTitle: string;
  action: string;
  foods = [
    { value: 'DICOTOMICA', viewValue: 'DICOTOMICA' },
    { value: 'UNICO', viewValue: 'SELECCION MULTIPLE' },
    // { value: 'MULTIPLE', viewValue: 'SELECCION MULTIPLE' },
    { value: 'RANGO', viewValue: 'RANGO NUMERICO' },
    { value: 'COMENTARIO', viewValue: 'COMENTARIO' }
  ];
  selectedFood = '';

  constructor(
    public dialogRef: MatDialogRef<QuestioFormComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
  ) { // Set the defaults
    this.action = _data.action;

    if (this.action === 'edit') {
      this.dialogTitle = 'EDITAR';
      this.newQuestion = _data.valor;
      this.selectedFood = this._data.valor.tipoalternativa;
      this.mostarchick=true;
      // this.alternativas = this._data.valor.alternativaList;
      this.botonname = 'EDITAR'

      // this.newQuestion.alternativaList
    }
    else {
      this.botonname = 'CREAR'

      this.dialogTitle = 'CREAR';
      this.newQuestion.tipoalternativa === 'DICOTOMICA'
      this.newQuestion = new QuestionsModel();
      this.newQuestion.alternativaList = [{descripcion:''}]
    }

  }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  seleccionado(valor) {
    this.mostarchick = false;
    this.alternativas = [];
    if (valor === 'COMENTARIO'){
      this.alternativas = [{descripcion:'COMENTARIO'}];
      this.newQuestion.alternativaList = [{descripcion:'COMENTARIO'}]
      this.bloquear = true;
    }else if(valor === 'RANGO') {
      this.newQuestion.alternativaList = [{descripcion:'RANGO'}]
      this.bloquear = true;
    } else {
      this.bloquear = false;
    }
      this.newQuestion.tipoalternativa = valor;
  }


  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    // Add our fruit
    if ((value || '').trim()) {
      this.alternativas.push({ descripcion: value.trim() });
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
    this.newQuestion.alternativaList = this.alternativas;
    if(this.alternativas.length === 2 && this.newQuestion.tipoalternativa === 'DICOTOMICA'){
      this.bloquear = true;
    }
  }

  remove(fruit): void {
    const index = this.alternativas.indexOf(fruit);
    if (index >= 0) {
      this.alternativas.splice(index, 1);
    }
    this.newQuestion.alternativaList = this.alternativas;
    if(this.alternativas.length === 1 && this.newQuestion.tipoalternativa === 'DICOTOMICA'){
      this.bloquear = false;
    }
  }
}
