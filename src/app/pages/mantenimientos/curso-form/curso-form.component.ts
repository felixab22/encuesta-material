import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EscuelaModel } from 'src/app/model/encuesta.model';
import { EncuestaService } from 'src/app/service/services.index';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { CursosModel } from '../../../model/encuesta.model';
@Component({
  selector: 'app-curso-form',
  templateUrl: './curso-form.component.html',
  styleUrls: ['./curso-form.component.scss']
})
export class CursoFormComponent implements OnInit {
  filteredOptions: Observable<EscuelaModel[]>;
  options = new Array<EscuelaModel>();
  action: string;
  contact: any;
  dialogTitle = 'NUEVO'
  validatingForm: FormGroup;
  series = [
    { id: 1, label: 'SERIE 100', value: "100" },
    { id: 2, label: 'SERIE 200', value: "200" },
    { id: 3, label: 'SERIE 300', value: "300" },
    { id: 4, label: 'SERIE 400', value: "400" },
    { id: 5, label: 'SERIE 500', value: "500" }
  ];
  constructor(
    public dialogRef: MatDialogRef<CursoFormComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    private _EncuestaSrv: EncuestaService,
    private _formBuilder: FormBuilder


  ) {
    // Set the defaults
    this.action = _data.action;
    if (this.action === 'edit') {
      this.dialogTitle = 'EDITAR';
      this.contact = _data.contact;
      this.validatingForm = this.createVarlidatortForm();
    }
    else {
      this.dialogTitle = 'CREAR';
      this.contact = new CursosModel();
      this.newValidatorForm();
    }

  }

  ngOnInit() {
    this.listEscuela();
    this.filteredOptions = this.validatingForm.get('myControl').valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.denominacion),
        map(denominacion => denominacion ? this._filter(denominacion) : this.options.slice())
      );
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  listEscuela() {
    this._EncuestaSrv.getAllEscuelas().subscribe((res: any) => {
      this.options = res.data;

    })
  }
  newValidatorForm() {
    this.validatingForm = new FormGroup({
      denominacion: new FormControl('', [Validators.required]),
      sigla: new FormControl('', [Validators.required]),
      myControl: new FormControl('', [Validators.required]),
      serie: new FormControl('', [Validators.required]),
    });
  }

  createVarlidatortForm(): FormGroup {
    return this._formBuilder.group({
      denominacion: [this.contact.denominacion],
      sigla: [this.contact.sigla],
      myControl: [this.contact.idescuela],
      serie: [this.contact.serie],
      idasignatura: [this.contact.idasignatura],
    });
  }
  private _filter(name: string): EscuelaModel[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.denominacion.toLowerCase().indexOf(filterValue) === 0);
  }
  displayFn(cursos: EscuelaModel): string {
    return cursos && cursos.denominacion ? cursos.denominacion : '';
  }
}
