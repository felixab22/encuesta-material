import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PersonalModel } from '../../../model/encuesta.model';
@Component({
  selector: 'app-personal-form',
  templateUrl: './personal-form.component.html',
  styleUrls: ['./personal-form.component.scss']
})
export class PersonalFormComponent implements OnInit {
  validatingForm: FormGroup;
  contact: any;
  dialogTitle: string;
  action: string;
  selectedCar = false;
  opciones= [
    {id:1, estado: true, descripcion:'ACTIVO', select: false},
    {id:2, estado: false, descripcion:'FINALIZADO', select: false}
  ]
  constructor(
    public dialogRef: MatDialogRef<PersonalFormComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    private _formBuilder: FormBuilder
  ) { // Set the defaults
    this.action = _data.action;
    
    if ( this.action === 'edit' )
    {
        this.dialogTitle = 'EDITAR';
        this.contact = _data.contact;
        this.validatingForm = this.createVarlidatortForm();
    }
    else
    {
        this.dialogTitle = 'CREAR';
        this.contact = new PersonalModel();
        this.newValidatorForm();
    }
    
      }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  createVarlidatortForm(): FormGroup
  {
      return this._formBuilder.group({
        cargo      : [this.contact.cargo],
        nombre    : [this.contact.nombre],
        apellido: [this.contact.apellido],
        dni  : [this.contact.dni],
        idpersona  : [this.contact.idpersona],
        telefono  : [this.contact.telefono],
        estado  : [this.contact.estado],
      });
  }
  newValidatorForm() {
    this.validatingForm = new FormGroup({
      cargo: new FormControl('',  [Validators.required]),
      estado: new FormControl('',  [Validators.required]),
      nombre: new FormControl('',  [Validators.required]),
      apellido: new FormControl('',  [Validators.required]),
      dni: new FormControl('',  [Validators.required, Validators.maxLength(8)])
    });
  }
}
