import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { DocentesModel } from '../../../model/encuesta.model';

@Component({
  selector: 'app-profesor-form',
  templateUrl: './profesor-form.component.html',
  styleUrls: ['./profesor-form.component.scss']
})
export class ProfesorFormComponent implements OnInit {
  validatingForm: FormGroup;
  contact: any;
  dialogTitle: string;
  action: string;
  
  constructor(
    public dialogRef: MatDialogRef<ProfesorFormComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    private _formBuilder: FormBuilder

  ) { 
// Set the defaults
this.action = _data.action;

if ( this.action === 'edit' )
{
    this.dialogTitle = 'EDITAR';
    this.contact = _data.contact;
}
else
{
    this.dialogTitle = 'CREAR';
    this.contact = new DocentesModel();
    this.newValidatorForm();
}

this.validatingForm = this.createVarlidatortForm();
  }

  ngOnInit() {
        
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  createVarlidatortForm(): FormGroup
  {
      return this._formBuilder.group({
        coddocente      : [this.contact.coddocente],
        nombre    : [this.contact.nombre],
        apellido: [this.contact.apellido],
        dni  : [this.contact.dni],
        idpersona  : [this.contact.idpersona],
      });
  }
  newValidatorForm() {
    this.validatingForm = new FormGroup({
      coddocente: new FormControl('',  [Validators.required]),
      nombre: new FormControl('',  [Validators.required]),
      apellido: new FormControl('',  [Validators.required]),
      dni: new FormControl('',  [Validators.required, Validators.maxLength(8)])
    });
  }
}
