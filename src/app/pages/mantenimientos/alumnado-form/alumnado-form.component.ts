import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { EscuelaModel, PersonalModel } from '../../../model/encuesta.model';
import { Observable } from 'rxjs';
import { EncuestaService } from 'src/app/service/services.index';
@Component({
  selector: 'app-alumnado-form',
  templateUrl: './alumnado-form.component.html',
  styleUrls: ['./alumnado-form.component.scss']
})
export class AlumnadoFormComponent implements OnInit {
  validatingForm: FormGroup;
  contact: any;
  dialogTitle: string;
  action: string;
  options = new Array<EscuelaModel>();
  filteredOptions: Observable<EscuelaModel[]>;
  afuConfig = {
    multiple: false,
    formatsAllowed: ".xlsx,.xls",
    maxSize: "5",
    uploadAPI:  {
      url:"https://example-file-upload-api",
      method:"POST",
      headers: {
     "Content-Type" : "text/plain;charset=UTF-8",
    //  "Authorization" : `Bearer ${token}`
      },
      params: {
        'page': '1'
      },
      responseType: 'blob',
    }
    
};




  constructor(
    public dialogRef: MatDialogRef<AlumnadoFormComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    private _formBuilder: FormBuilder,
    private _EncuestaSrv: EncuestaService,

  ) {
    this.action = _data.action;
    
    if ( this.action === 'edit' )
    {
        this.dialogTitle = 'EDITAR';
        this.contact = _data.contact;
        this.validatingForm = this.createVarlidatortForm();
    }
    else
    {
        this.dialogTitle = 'CREAR';
        this.contact = new PersonalModel();
        this.newValidatorForm();
    }
   }

  ngOnInit() {
    this.listEscuela();    
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  listEscuela(){
    this._EncuestaSrv.getAllEscuelas().subscribe((res:any)=> {
      this.options = res.data;
        
    })
  }
  createVarlidatortForm(): FormGroup
  {
      return this._formBuilder.group({
        nombre    : [this.contact.nombre],
        apellido: [this.contact.apellido],
        dni  : [this.contact.dni],
        idpersona  : [this.contact.idpersona],
        // myControl: [this.contact.idescuela],
        correo  : [this.contact.correo],
        codigo  : [this.contact.codigo]
      });
  }
  newValidatorForm() {
    this.validatingForm = new FormGroup({
      nombre: new FormControl('',  [Validators.required]),
      codigo: new FormControl('',  [Validators.required]),
      apellido: new FormControl('',  [Validators.required]),
      dni: new FormControl('',  [Validators.required, Validators.maxLength(8)]),
      // myControl: new FormControl('', [Validators.required]),
      correo: new FormControl('@unsch.edu.pe')
    });
  }
}
