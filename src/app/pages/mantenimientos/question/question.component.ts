import { Component, OnInit } from '@angular/core';
import { EncuestaModel } from 'src/app/model/encuesta.model';
import { EncuestaService } from 'src/app/service/services.index';
import { MatDialog } from '@angular/material/dialog';
import { QuestioFormComponent } from '../questio-form/questio-form.component';
import { QuestionsModel } from '../../../model/encuesta.model';
import { QuestioAlterComponent } from '../questio-alter/questio-alter.component';

declare var swal: any;

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
  opction = 'DOCENTE';
  panelOpenState = false;

  buscar = '';
  preguntas = new Array<QuestionsModel>();
  newPregunta = new QuestionsModel();
  mostrarboton = false;
  radioselect = 'DOCENTE';
  idencuet: 0;
  alternativas = [
    {
      descripcion: "",
      idpregunta: 0
    }]

  currentCategory = 0;
  categories = new Array<EncuestaModel>();
  constructor(
    private _EncuestaSrv: EncuestaService,
    public dialog: MatDialog,

  ) { }

  ngOnInit() {
    this.listaEncuesta();
    this.listaAllPregunta();
  }
  listaEncuesta() {
    this._EncuestaSrv.getAllEncuesta().subscribe((res: any) => {
      this.categories = res.data;
    })
  }
  listaAllPregunta() {
    this._EncuestaSrv.getAllPreguntas(this.radioselect).subscribe((res: any) => {
      if (res.code === 200) {
        this.preguntas = res.data;
      }
    })
  }

  saveOrUpdatePregunta(valor, alternativas, tipo) {
    this._EncuestaSrv.saveOrUpdatePregunta(valor).subscribe((res: any) => {
      if (res.code === 200) {
        this.newPregunta = new QuestionsModel();
        if (tipo === 'new') {
          this.saveOrUpdateAlternativaAndPregunta(alternativas, res.data.idpregunta)
        } else {
           swal('BIEN!', 'PREGUNTA EDITADO CORRECTAMENTE!', 'success');
        }
      }
    })

  }
  saveOrUpdateAlternativaAndPregunta(valor, idpregunta) {
    this.alternativas = [
      {
        descripcion: "",
        idpregunta: 0
      }];
    for (let i = 0; i < valor.length; i++) {
      if (i === 0) {
        this.alternativas[0].descripcion = valor[0].descripcion;
        this.alternativas[0].idpregunta = idpregunta;
      } else {
        this.alternativas.push({ idpregunta: idpregunta, descripcion: valor[i].descripcion })
      }
    }
    this._EncuestaSrv.saveOrUpdateAlternativa(this.alternativas).subscribe((res: any) => {
      if (res.code === 200) {
        swal('Bien!', 'Guardado!', 'success');
        this.listaAllPregunta();
      }
    })
  }
  editarAlternativa(item, a) {
    a = { descripcion: a.descripcion, idalternativa: a.idalternativa, idpregunta: item }
    const dialogRef = this.dialog.open(QuestioAlterComponent, {
      width: '550px',
      data: a
    });
    dialogRef.afterClosed().subscribe(result => {
      result = [result];
      this._EncuestaSrv.saveOrUpdateAlternativa(result).subscribe((res: any) => {
        if (res.code === 200) {
          swal('Bien!', 'Guardado!', 'success');
          this.listaAllPregunta();
        }
      })

    });


  }
  editarEncuesta(valor = null) {
    var entrada = 'edit'
    if (valor === null) {
      entrada = 'new'
    }
    const dialogRef = this.dialog.open(QuestioFormComponent, {
      width: '550px',
      data: {
        valor: valor,
        action: entrada
      }
    });
    dialogRef.afterClosed().subscribe(result => {

      if (result !== undefined) {
        if (entrada === 'edit') {
          this.newPregunta.idpregunta = result.idpregunta;
        }
        this.newPregunta.tipoalternativa = result.tipoalternativa;
        this.newPregunta.tipopregunta = result.tipopregunta;
        this.newPregunta.enunciado = result.enunciado;
        this.newPregunta.estado = false;
        this.saveOrUpdatePregunta(this.newPregunta, result.alternativaList, entrada);
      }
    });
  }
}
