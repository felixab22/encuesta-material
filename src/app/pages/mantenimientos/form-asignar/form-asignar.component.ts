import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CargaAcademicaModel, DocentesModel, CursosModel } from '../../../model/encuesta.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EncuestaService } from 'src/app/service/services.index';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
@Component({
  selector: 'app-form-asignar',
  templateUrl: './form-asignar.component.html',
  styleUrls: ['./form-asignar.component.scss']
})
export class FormAsignarComponent implements OnInit {
  newCarga = new Array<CargaAcademicaModel>();
  validatingForm: FormGroup;
  options = new Array<CursosModel>();
  semestres = [
    { value: '2020-I', label: '2020-I' },
    { value: '2020-II', label: '2020-II' },
    { value: '2021-I', label: '2021-I' },
    { value: '2021-II', label: '2021-II' },
    { value: '2022-I', label: '2022-I' },
    { value: '2022-II', label: '2022-II' },
  ];
  subir = 0;
  mandar: Array<any> = [{ tipo: '', idasignatura: '', iddocente: 0, semestre: '', asignatura: '' }];
  filteredOptions: Observable<CursosModel[]>;
  constructor(
    public dialogRef: MatDialogRef<FormAsignarComponent>,
    @Inject(MAT_DIALOG_DATA) public docente: DocentesModel,
    private _EncuestaSrv: EncuestaService,

  ) {     
  }

  ngOnInit() {
    this.subir = 0;
    this.mandar= [{ tipo: '', idasignatura: '', iddocente: 0, semestre: '', asignatura: '' }];
    this.validatingForm = new FormGroup({
      semestre: new FormControl('', [Validators.required]),
      myControl: new FormControl('', [Validators.required]),
      labo: new FormControl(false),
      teo: new FormControl(false),
      prac: new FormControl(false),
    });

    this.filteredOptions = this.validatingForm.get('myControl').valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.denominacion),
        map(denominacion => denominacion ? this._filter(denominacion) : this.options.slice())
      );
    this.listarCursos();
  }
  listarCursos() {
    this._EncuestaSrv.getAllAsignatura().subscribe((res: any) => {
      this.options = res.data;

    })
  }
  displayFn(cursos: CursosModel): string {
    return cursos && cursos.denominacion ? cursos.denominacion : '';
  }

  private _filter(name: string): CursosModel[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.denominacion.toLowerCase().indexOf(filterValue) === 0);
  }


  onNoClick(): void {
    this.dialogRef.close();
  }
  eliminarCursos(item) {
    this.mandar.splice(item, 1);

  }
  saveOrUpdateCargo(valor: any) {

    if (valor.labo === true) {
      var temp = { tipo: '', idasignatura: '', iddocente: 0, semestre: '', asignatura: '' };
      temp.idasignatura = valor.myControl.idasignatura;
      temp.asignatura = valor.myControl.denominacion;
      temp.semestre = valor.semestre;
      temp.iddocente = this.docente.idpersona;
      temp.tipo = 'LABORATORIO'
      this.mandar.push(temp);
    }
    if (valor.teo === true) {
      var temp = { tipo: '', idasignatura: '', iddocente: 0, semestre: '', asignatura: '' };
      temp.idasignatura = valor.myControl.idasignatura;
      temp.asignatura = valor.myControl.denominacion;
      temp.semestre = valor.semestre;
      temp.iddocente = this.docente.idpersona;
      temp.tipo = 'TEORIA'
      this.mandar.push(temp);
    }
    if (valor.prac === true) {
      var temp = { tipo: '', idasignatura: '', iddocente: 0, semestre: '', asignatura: '' };
      temp.idasignatura = valor.myControl.idasignatura;
      temp.asignatura = valor.myControl.denominacion;
      temp.semestre = valor.semestre;
      temp.iddocente = this.docente.idpersona;
      temp.tipo = 'PRACTICA'
      this.mandar.push(temp);
    }
    if (this.subir === 0) {
      this.mandar.shift();
      this.subir = 1;
    }
    // formDirective.resetForm() //reset del form

  }
  ngOnDestroy(): void {
    this.subir = 0;
    this.mandar= [{ tipo: '', idasignatura: '', iddocente: 0, semestre: '', asignatura: '' }];
  }

}
