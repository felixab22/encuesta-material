import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlumnadoComponent } from './alumnado/alumnado.component';
import { CargaComponent } from './carga/carga.component';
import { CursosComponent } from './cursos/cursos.component';
import { MantenimientoComponent } from './mantenimiento/mantenimiento.component';
import { ProfesoresComponent } from './profesores/profesores.component';
import { QuestionComponent } from './question/question.component';
import { PersonalComponent } from './personal/personal.component';


const routes: Routes = [
  {
    path: 'Lista',
    component: MantenimientoComponent
  },
  {
    path: 'Profesores',
    component: ProfesoresComponent
  },
  {
    path: 'Estudiantes',
    component: AlumnadoComponent
  },
  {
    path: 'Questions',
    component: QuestionComponent
  },
  {
    path: 'Cursos',
    component: CursosComponent
  },
  {
    path: 'Carga',
    component: CargaComponent
  },
  {
    path: 'Administrativo',
    component: PersonalComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MantenimientosRoutingModule { }
