import { Component, OnInit, ViewChild } from '@angular/core';
import { EncuestaService } from 'src/app/service/services.index';
import { CursoFormComponent } from '../curso-form/curso-form.component';
declare var swal: any;
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.scss']
})
export class CursosComponent implements OnInit {
  buscar = '';
  done = [];
  allComplete: boolean = false;

  ideliminar = [{ idasignatura: 0 }];
  displayedColumns: string[] = ['denominacion', 'sigla', 'serie'];
  dataSource = new MatTableDataSource<any>(this.done);
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  mostrarboton = false;
  constructor(
    private _EncuestaSrv: EncuestaService,
    public dialog: MatDialog,

  ) { }

  ngOnInit() {
    
    this.listCursos();

  }
  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  editarcurso(valor = null) {
    var entrada = 'edit'
    if (valor === null) {
      entrada = 'new'
    }
    const dialogRef = this.dialog.open(CursoFormComponent, {
      width: '550px',
      data: {
        contact: valor,
        action: entrada
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result !== undefined){
        if(valor === null){

          this.saveOrUpdateCurso(result);
        } else {
          this.saveOrUpdateCursoEdit(result);
          
        }
      }
    });
  }
  saveOrUpdateCurso(valor: any) {
      var mandar = { denominacion: valor.denominacion, sigla: valor.sigla, idescuela: valor.myControl.idescuela, serie:valor.serie }    
    this._EncuestaSrv.saveOrUpdateAsignatura(mandar).subscribe((res: any) => {
      if (res.code === 200) {
        this.done.push(res.data);
        swal('Bien!', 'Guardado!', 'success');
      }
    }
    )
  }
  saveOrUpdateCursoEdit(valor: any) {
     var mandar = {idasignatura:valor.idasignatura, denominacion: valor.denominacion, sigla: valor.sigla, idescuela: valor.myControl.idescuela, serie:valor.serie }
         
    this._EncuestaSrv.saveOrUpdateAsignatura(mandar).subscribe((res: any) => {
      if (res.code === 200) {
        this.listCursos();
        swal('Bien!', 'Guardado!', 'success');
      }
    }
    )
  }
  updateAllComplete() {
    this.allComplete = this.done != null && this.done.every(t => t.completed);
  }
  listCursos() {
    this._EncuestaSrv.getAllAsignatura().subscribe((res: any) => {
      if(res.code === 200){
        this.done = res.data;
        this.dataSource = new MatTableDataSource<any>(this.done);
      }

    })
  }



  seleccionDelete() {
    // this.ideliminar = [{ idasignatura: 0 }];
    // var i = 0;
    // do {
    //   if (this.task.subtasks[i].estado === true) {
    //     if (this.ideliminar[0].idasignatura === 0) {
    //       this.ideliminar[0].idasignatura = this.task.subtasks[i].idasignatura;
    //       i++;
    //     } else {
    //       var mandar = { idasignatura: 10 };
    //       mandar.idasignatura = this.task.subtasks[i].idasignatura;
    //       this.ideliminar.push(mandar);
    //       i++;
    //     }
    //   } else {
    //     i++;
    //   }
    // } while (i < this.task.subtasks.length)

  }


}
