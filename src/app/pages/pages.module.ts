import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { SharedModule } from '../shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
// import { CustomMaterialModule } from '../configuration/CustomMaterial.module';
import { AppRoutingModule } from '../app-routing.module';



@NgModule({
  declarations: [PagesComponent],
  imports: [
    CommonModule,
    SharedModule,
    BrowserModule,
    AppRoutingModule,    // esto es para el router-ouleth
    // CustomMaterialModule
  ]
})
export class PagesModule { }
