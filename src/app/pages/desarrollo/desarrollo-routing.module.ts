import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { PreguntaComponent } from './pregunta/pregunta.component';
import { DocenteComponent } from './docente/docente.component';
import { AdministrativoComponent } from './administrativo/administrativo.component';
import { DocenteFormComponent } from './docente-form/docente-form.component';

const routes: Routes = [
  {
    path: 'Docentes',
    component: DocenteComponent
  },
  {
    path: 'Pregunta',
    component: PreguntaComponent
  },
  {
    path: 'Inicio',
    component: InicioComponent
  },
  {
    path: 'Administrativo',
    component: AdministrativoComponent
  },
  {
    path: 'Cursos',
    component: DocenteFormComponent
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DesarrolloRoutingModule { }

