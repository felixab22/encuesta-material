import { Component, OnInit } from '@angular/core';
import { PersonalModel } from '../../../model/encuesta.model';
import { EncuestaService } from 'src/app/service/services.index';

@Component({
  selector: 'app-administrativo',
  templateUrl: './administrativo.component.html',
  styleUrls: ['./administrativo.component.scss']
})
export class AdministrativoComponent implements OnInit {
  buscar='';
  listarAdminis = new Array<PersonalModel>();
  constructor(
    private _EncuestaSrv: EncuestaService
  ) { }

  ngOnInit() {
    this.listarAdministrativos();
  }  
 
  listarAdministrativos() {
    this._EncuestaSrv.getAllPersonal('PERSONAL').subscribe((res: any) => {
      this.listarAdminis = res.data;
    })
  }
}
