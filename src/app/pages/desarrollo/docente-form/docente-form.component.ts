import { Component, OnInit } from '@angular/core';
import { DocentesModel } from '../../../model/encuesta.model';
import { EncuestaService } from 'src/app/service/services.index';
declare var swal: any;
@Component({
  selector: 'app-docente-form',
  templateUrl: './docente-form.component.html',
  styleUrls: ['./docente-form.component.scss']
})
export class DocenteFormComponent implements OnInit {
  docente = new DocentesModel();
  listaDocentes: any;

  constructor(
    private _EncuestaSrv: EncuestaService,

  ) { }

  ngOnInit() {
 
  }
 

  listarDocentes(id) {
    this._EncuestaSrv.getAllPCargaAcademicaXdocente(id).subscribe((res: any) => {
      this.listaDocentes = res.data;
    })
  }
  eliminarCurso(id){

  }

  
}
