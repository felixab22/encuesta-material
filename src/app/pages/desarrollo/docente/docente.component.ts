import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { EncuestaService } from '../../../service/encuesta.service';
import { DocentesModel } from './../../../model/encuesta.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-docente',
  templateUrl: './docente.component.html',
  styleUrls: ['./docente.component.scss']
})
export class DocenteComponent implements OnInit {
  currentCategory = 'todos';
  currentSerie = 'TODOS';

  categories = [
    { id: 1, label: 'TEORIA', value: 'TEORIA' },
    { id: 2, label: 'LABORATORIO', value: 'LABORATORIO' },
    { id: 3, label: 'PRACTICA', value: 'PRACTICA' }
  ];
  series = [
    { id: 1, label: 'SERIE 100', value: "100" },
    { id: 2, label: 'SERIE 200', value: "200" },
    { id: 3, label: 'SERIE 300', value: "300" },
    { id: 4, label: 'SERIE 400', value: "400" },
    { id: 5, label: 'SERIE 500', value: "500" }
  ];
  coursesFilteredByCategory: any[];
  filtertipo: any[];
  buscar='';
  URL = 'localstorage' + '/upload';
  documentnames: Array<any>;
  public uploader: FileUploader = new FileUploader({ url: this.URL });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  done: any;
  newDocente: DocentesModel;
  tipoboton = 'CREAR';
  constructor(
    private _EncuestaSrv: EncuestaService,
    private _router: Router

  ) {
    this.newDocente = new DocentesModel();
  }

  ngOnInit() {
  
   this.listarDocentes();
  }
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
    for (let i = 0; i < this.uploader.queue.length; i++) {
      this.uploader.queue[i].file.name = 'profesires-' + this.uploader.queue[i].file.name;
    }
  }
  crearDocente() {
    this._EncuestaSrv.saveOrUpdateDocente(this.newDocente).subscribe((res: any) => {
      if (res.code === 200) {
        localStorage.setItem('docente',JSON.stringify(res.data));
        this._router.navigate(['/Sistemas/Desarrollo/Cursos'])
        // this.listarDocentes();
        this.newDocente = new DocentesModel();
        this.tipoboton = 'CREAR';
      }
    });
  }
  EditarDocente(docente) {
    this.newDocente = docente;
    this.tipoboton = 'EDITAR';
  }
  listarDocentes() {
    var tipo = JSON.parse(localStorage.getItem('encuesta'));
    if(tipo.tipoencuesta === 'DOCENTE') {
      this._EncuestaSrv.getAllPCargaAcademicaXSemestre(tipo.semestre).subscribe((res: any) => {
        this.coursesFilteredByCategory=this.filtertipo = this.done = res.data;
      })
    }
  }
  filterCoursesByCategory(): void {
    // Filter
    if (this.currentCategory === 'todos') {
      this.coursesFilteredByCategory = this.done;
      this.filtertipo = this.done;

    }
    else {
      this.coursesFilteredByCategory = this.done.filter((course) => {
        return course.tipo === this.currentCategory;
      });

      this.filtertipo = [...this.coursesFilteredByCategory];

    }

    // Re-filter by search term
  }
  filterCoursesBySerie(): void {
    // Filter
    if (this.currentSerie === 'todos') {
      this.coursesFilteredByCategory = this.done;
      this.filtertipo = this.done;
    }
    else {
      this.coursesFilteredByCategory = this.done.filter((course) => {
        return course.serie === this.currentSerie;
      });
      this.filtertipo = [...this.coursesFilteredByCategory];
    }

    // Re-filter by search term
  }
  EliminarDocente(idcarga: number): void {
    if (confirm('¿Está seguro que desea eliminar el Area?')) {
      this._EncuestaSrv.deleteCargaAcademica(idcarga);
      // swal('Eliminado!', 'Area Eliminado!');
      this.filtertipo = this.filtertipo.filter(u => idcarga !== u.idcarga);
    }
  }
  CargaAcademica(item){
    localStorage.setItem('docente',JSON.stringify(item));
        this._router.navigate(['/Sistemas/Desarrollo/Cursos'])
  }
  
}
