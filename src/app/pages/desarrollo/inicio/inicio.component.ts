import { Component, OnInit, ViewChild } from '@angular/core';

import { EncuestaService } from 'src/app/service/services.index';
import { EncuestaModel } from '../../../model/encuesta.model';
import { MatDialog } from '@angular/material/dialog';
import { InicioModalComponent } from '../inicio-modal/inicio-modal.component';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  listaencuesta: EncuestaModel[];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  buscar = ''
  constructor(
    private _EncuestaSrv: EncuestaService,
    public dialog: MatDialog,
    private _router: Router
  ) {

  }

  ngOnInit() {
    this.listarEncuesta();
  }

  openDialog(): void {    
    const dialogRef = this.dialog.open(InicioModalComponent, {
      width: '650px',
      data: {
        valor: null,
        action: 'new'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result !== undefined){
        this.crearNuevaEncuesta(result);
      }
    });
  }
  crearPreguntas(element) {
    localStorage.setItem('encuesta', JSON.stringify(element));
    this._router.navigate(['/Sistemas/Desarrollo/Pregunta']);

  }
  crearDocentes(item) {
    localStorage.setItem('encuesta', JSON.stringify(item));
    if(item.tipoencuesta === 'DOCENTE') {
      this._router.navigate(['/Sistemas/Desarrollo/Docentes']);
    } else if(item.tipoencuesta === 'ADMINISTRATIVO') {
      this._router.navigate(['/Sistemas/Desarrollo/Administrativo']);

    }

  }

  listarEncuesta() {
    this._EncuestaSrv.getAllEncuesta().subscribe((res: any) => {
      if (res.code === 200) {
        this.listaencuesta = res.data;
      } else {

      }
    })
  }
  crearNuevaEncuesta(NewEncuesta) {
    this._EncuestaSrv.saveOrUpdateEncuesta(NewEncuesta).subscribe((res: any) => {
      this.listarEncuesta();
    })
  }
  editarEncuesta(valor:null){
    var entrada = 'edit'
    if (valor === null) {
      entrada = 'new'
    }
    const dialogRef = this.dialog.open(InicioModalComponent, {
      width: '650px',
      data: {
        valor: valor,
        action: entrada
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result !== undefined){
        this.crearNuevaEncuesta(result);
      }
    });
  }
  reportes(valor){
    if(valor.tipoencuesta==='DOCENTE'){
      localStorage.setItem('graficarde',JSON.stringify(valor));
      this._router.navigate(['/Sistemas/Resultados/Docente']);
    } else if(valor.tipoencuesta==='ADMINISTRATIVO'){
      localStorage.setItem('graficarde',JSON.stringify(valor));
      this._router.navigate(['/Sistemas/Resultados/Administrativo']);
    }
  }
}
