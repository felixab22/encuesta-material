import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesarrolloRoutingModule } from './desarrollo-routing.module';
import { CustomMaterialModule } from 'src/app/configuration/CustomMaterial.module';
import { InicioComponent } from './inicio/inicio.component';
import { FileUploadModule } from 'ng2-file-upload';
import { ServicesModule } from 'src/app/service/services.module';
import { PreguntaComponent } from './pregunta/pregunta.component';
import { DocenteComponent } from './docente/docente.component';
import { InicioModalComponent } from './inicio-modal/inicio-modal.component';
import { AdministrativoComponent } from './administrativo/administrativo.component';
import { DocenteFormComponent } from './docente-form/docente-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { MantenimientosModule } from '../mantenimientos/mantenimientos.module';


@NgModule({
  declarations: [InicioComponent, PreguntaComponent, DocenteComponent, InicioModalComponent, AdministrativoComponent, DocenteFormComponent],
  imports: [
    CommonModule,
    DesarrolloRoutingModule,
    ReactiveFormsModule,
    CustomMaterialModule,
    FileUploadModule,
    ServicesModule,
    Ng2SearchPipeModule,
    MantenimientosModule
  ],
  bootstrap: [
    InicioModalComponent,
    
  ]
})
export class DesarrolloModule { }
