import { Component, OnInit, Inject } from '@angular/core';
import { EncuestaModel } from 'src/app/model/encuesta.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-inicio-modal',
  templateUrl: './inicio-modal.component.html',
  styleUrls: ['./inicio-modal.component.scss']
})
export class InicioModalComponent implements OnInit {
  NewEncuesta: EncuestaModel;
  dialogTitle = 'NUEVO';
  semestres = [
    { value: '2020-I', label: '2020-I' },
    { value: '2020-II', label: '2020-II' },
    { value: '2021-I', label: '2021-I' },
    { value: '2021-II', label: '2021-II' },
    { value: '2022-I', label: '2022-I' },
    { value: '2022-II', label: '2022-II' },
  ];
  picker: any;
  picker2: any;
  fromDate: any;
  fromDate2: any;
  tipoencuesta:any = [
    {value: 'DOCENTE', viewValue: 'DOCENTE'},
    {value: 'ADMINISTRATIVO', viewValue: 'ADMINISTRATIVO'},
    {value: 'ESCUELA', viewValue: 'ESCUELA'}
  ];
  estado:any = [
    {value: true, viewValue: 'ACTIVO'},
    {value: false, viewValue: 'FINALIZADO'},

  ];
  constructor(
    public dialogRef: MatDialogRef<InicioModalComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any
  ) {
    if(this._data.action === 'new') {
      this.dialogTitle = 'NUEVO';
      this.NewEncuesta = new EncuestaModel();
      this.NewEncuesta.fcreacion = new Date()
    } else {
      this.dialogTitle = 'EDITAR';
      this.NewEncuesta = this._data.valor;
    }
  }

  ngOnInit() {
    
  }
  onChangeEvent(e): void {
    this.NewEncuesta.finicio = e.target.value;
  }
  onChangeEvent2(e): void {
    this.NewEncuesta.ffin = e.target.value;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
