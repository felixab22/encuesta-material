import { Component, OnInit, ViewChild } from '@angular/core';
import { QuestionsModel, QuestionsResponseModel, Task } from '../../../model/encuesta.model';
import { EncuestaService } from 'src/app/service/services.index';
import { MatDialog } from '@angular/material/dialog';
import { QuestioFormComponent } from '../../mantenimientos/questio-form/questio-form.component';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

declare var swal:any;
@Component({
  selector: 'app-pregunta',
  templateUrl: './pregunta.component.html',
  styleUrls: ['./pregunta.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class PreguntaComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  done = [];
  displayedColumns: string[] = ['select', 'pregunta', 'tipo'];
  expandedElement: any | null;

  panelOpenState = false;  
  alternativas = [
    {
      descripcion: "",
      idpregunta: 0
    }]
  buscar = ''
  allComplete: boolean = false;
  mostrarboton = true;
  listpreguntas = [{ idpregunta: 0, idencuesta: 0}];
  ideliminar  = [{ idpregunta: 0 }];
  ListaPregunta: QuestionsResponseModel[];
  newPregunta: QuestionsModel
  encuesta: any;
  tituloboton = 'CREAR';
  radioselect = 'DOCENTE';
  verborrar = false;
  currentCategory = 0;  
  // enunciada = new FormControl('', [Validators.required, Validators.minLength(10)]);
  task: Task = {
    enunciado: 'SELECCIONAR TODOS',
    estado: false,
    color: 'primary',
    idpregunta: 0,
    subtasks: [
      { idpregunta: 1, enunciado: 'SIN PREGUNTAS', estado: true }
    ]
  };
  dataSource = new MatTableDataSource<any>(this.task.subtasks);
  constructor(
    private _EncuestaSrv: EncuestaService,
    public dialog: MatDialog,

  ) {
    this.newPregunta = new QuestionsModel();
    this.encuesta = JSON.parse(localStorage.getItem('encuesta'));
  }

  ngOnInit() {
    this.listarPreguntas();
    this.listaAllPregunta(this.encuesta.tipoencuesta);

  }
 
  listarPreguntas() {
    this._EncuestaSrv.getAllPreguntaXidencuesta(this.encuesta.idencuesta).subscribe((res: any) => {
      if (res.code === 200) { 
            
        this.ListaPregunta = res.data
      } else {
        console.error('lista vacía');

      }
    });
  }
  listaAllPregunta(tipo) {
    this._EncuestaSrv.getAllPreguntas(tipo).subscribe((res: any) => {
      if(res.code === 200){
        this.done = this.task.subtasks = res.data;
        this.dataSource = new MatTableDataSource<any>(this.task.subtasks);
      } else {
        console.log('listas sin registros');        
      }

    })
  } 
  updateAllComplete(valor) {
    
    this.allComplete = this.task.subtasks != null && this.task.subtasks.every(t => {
      t.estado      
      if(t.estado === true || valor.estado === true){
        this.mostrarboton = false;
      } else {
        this.mostrarboton = true;

      }
    } 
    );
    
  }

  someComplete(): boolean {
    if (this.task.subtasks == null) {
      return false;
    }
    return this.task.subtasks.filter(t => t.estado).length > 0 && !this.allComplete;
  }

  setAll(estado: boolean) {
    this.allComplete = estado;
    this.mostrarboton = !this.mostrarboton;    
    if (this.task.subtasks == null) {
      return;
    }
    this.task.subtasks.forEach(t => t.estado = estado);
  }
  seleccionarPregunta(){
    this.listpreguntas = [{ idpregunta: 0, idencuesta: 0}];
    var i = 0;
    do {
      if (this.task.subtasks[i].estado === true) {
        if (this.listpreguntas[0].idpregunta === 0) {
          this.listpreguntas[0].idpregunta = this.task.subtasks[i].idpregunta;
          this.listpreguntas[0].idencuesta = this.encuesta.idencuesta;
          i++;
        } else {
            var mandar = { idpregunta: 10, idencuesta: 0 };
            mandar.idpregunta = this.task.subtasks[i].idpregunta;
            mandar.idencuesta = this.encuesta.idencuesta;
            this.listpreguntas.push(mandar);
            i++;
        }
      } else {
        i++;
        
      }
    } while (i < this.task.subtasks.length);    
    this._EncuestaSrv.saveOrUpdateEncuestaPregunta(this.listpreguntas).subscribe((res:any)=> {
      if(res.code === 200){
         swal('BIEN!', 'PREGUNTAS IMPORTADOS A LA ENCUESTA!', 'success');
      }
      
      this.listarPreguntas();
    })
  }
  seleccionDelete() {
    this.ideliminar = [{ idpregunta: 0}];
    var i = 0;
    do {
      if (this.task.subtasks[i].estado === true) {
        if (this.ideliminar[0].idpregunta === 0) {
          this.ideliminar[0].idpregunta = this.task.subtasks[i].idpregunta;
          i++;
        } else {
            var mandar = { idpregunta: 10};
            mandar.idpregunta = this.task.subtasks[i].idpregunta;
            this.ideliminar.push(mandar);
            i++;
        }
      } else {
        i++;        
      }
    } while (i < this.task.subtasks.length)

  }
  editarEncuesta(valor = null) {
    var entrada = 'edit'
    if (valor === null) {
      entrada = 'new'
    }
    const dialogRef = this.dialog.open(QuestioFormComponent, {
      width: '550px',
      data: {
        contact: valor,
        action: entrada
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if(entrada === 'edit') {
          this.newPregunta = new QuestionsModel();
        }
        this.newPregunta.tipoalternativa = result.tipoalternativa;
        this.newPregunta.tipopregunta = result.tipopregunta;
        this.newPregunta.enunciado = result.enunciado;
        this.newPregunta.estado = false;       
        this.saveOrUpdatePregunta(this.newPregunta, result.alternativaList);
      }
    });
  }
  saveOrUpdatePregunta(valor, alternativas) {
    this._EncuestaSrv.saveOrUpdatePregunta(valor).subscribe((res: any) => {
      if (res.code === 200) {
        this.newPregunta = new QuestionsModel();        
        this.saveOrUpdateAlternativaAndPregunta(alternativas, res.data.idpregunta)
      }
    })

  }
  saveOrUpdateAlternativaAndPregunta(valor, idpregunta) {
    this.alternativas = [
      {
        descripcion: "",
        idpregunta: 0
      }];
    for (let i = 0; i < valor.length; i++) {
      if (i === 0) {
        this.alternativas[0].descripcion = valor[0].descripcion;
        this.alternativas[0].idpregunta = idpregunta;
      } else {
        this.alternativas.push({ idpregunta: idpregunta, descripcion: valor[i].descripcion })
      }
    }    
      this._EncuestaSrv.saveOrUpdateAlternativa(this.alternativas).subscribe((res:any)=>{
        if(res.code === 200){
        this.listaAllPregunta(this.encuesta.tipoencuesta);
        this.listpreguntas = [{ idpregunta: 0, idencuesta: 0}];
        this.listpreguntas[0].idpregunta = idpregunta;
        this.listpreguntas[0].idencuesta = this.encuesta.idencuesta;
        this._EncuestaSrv.saveOrUpdateEncuestaPregunta(this.listpreguntas).subscribe((res:any)=> {
          this.listarPreguntas();
        });
        swal('Bien!', 'Guardado!', 'success');
        }
      })
  }
}

