import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
verimagen = true;
imagen = './assets/img/inicio-2.png' || "/../assets/img/inicio-2.png";
imagen2 = './assets/img/inicio-movile.png' || "/../assets/img/inicio-movile.png";


  ngOnInit() {
    this.setDelay();
  }
  setDelay() {
    setTimeout(() => {
      this.verimagen = false;
    }, 1500);
  }  
}
