import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { URL_SERVICIOS } from '../configuration/ip.configuration';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AlumnoAuthService {
    api = URL_SERVICIOS;
    constructor(
        private http: HttpClient
    ) {

    }
    // ========================== Encuesta ==============================
    // listar
    attemptAuthAlumno(usernameOrEmail: string, password: string): Observable<any> {
        const credentials = { usernameOrEmail: usernameOrEmail, password: password };
        return this.http.post(this.api + '/auth/authEstudiante', credentials).pipe(map(res => {
            return res;
        }))
    }

}
