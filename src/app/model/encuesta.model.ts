import { ThemePalette } from '@angular/material/core';

export class EncuestaModel {
    idencuesta: number;
    denominacion: string;
    finicio: Date;
    ffin: Date;
    fcreacion: Date;
    semestre: string;
    tipoencuesta: string;
    estado: boolean;
}
export class PreguntaModel {
    idpregunta: number;
    enunciado: string;
    estado: boolean;
}
export class QuestionsModel {
    idencuestapregunta?: number;
    enunciado: string;
    estado: boolean;
    tipopregunta: any;
    alternativaList?: any;
    tipoalternativa: any;
    idpregunta: any;
}
export class QuestionsResponseModel {
    idencuestapregunta?: number;
    pregunta: {
        idpregunta: number;
        enunciado: string;
        estado: boolean;
        tipopregunta: string,
        tipoalternativa: string,
        alternativaList: Array<AlternativaModel>;
    }
}

export class GraficandoModel {
    descrip?: string;
    val1?: string;
    val2?: string;
    val3?: string;
    val4?: string;
    val5?: string;
    large1?:number;
    large2?:number
    large3?:number
    large4?:number
    large5?:number
    idrespuesta?:number
}

export class RespuestaModel {
    idrespuesta?: number;
    descripcion: string;
    ponderacion: number;
    cargaacademica: any;
    idencuestapregunta: any;
}
export class CargaAcademicaModel {
    idcarga?: number;
    tipo: string;
    idasignatura: any;
    semestre: string;
    iddocente?: any;
}
export class AlternativaModel {
    idpregunta?: any;
    idalternativa: number;
    descripcion: string;
}
export class LoginModel {
    username: string;
    password: string;
}
export class EncuestadorModel {
    idencuestado: number;
    fencuesta: Date;
    idencuesta: any;
    idpersona?: any;
    idestudiante?: any;
}
export class PersonalModel {
    idpersona?: number;
    nombre?: string;
    apellido?: string;
    dni?: string;
    cargo: string;
    telefono?: string;
    estado: boolean;
    correo: string;
    idescuela?: any;
    codigo?: any;
}
export class DocentesModel {
    nombre: string;
    apellido: string;
    coddocente: string;
    iddocente?: any;
    dni?: any;
    idpersona?: any;
}
export class CursosModel {
    denominacion: string;
    sigla: string;
    idasignatura: any;
    idescuela: number;
    myControl?: any;
}
export interface Task {
    enunciado?: string;
    estado?: boolean;
    color?: ThemePalette;
    subtasks?: Task[];
    idpregunta?: number;
    idasignatura?: number;
    denominacion?: string;
    idescuela?: number;
    sigla?: string;
}
export class EscuelaModel {
    idescuela: number;
    codigo: number;
    denominacion: string;
}
export class GraficaModel {
    asignatura: string;
    modalidadcurso: string;
    sigla: string;
    idcarga: number;
    idrespuesta?: number;
    enunciadopregunta: string;
    idpregunta: number;
    tipoAlternativa: string;
    calificacion: string;
    cantidad: number;

}
export class ComentarioModel {
    asignatura: string;
    comentario: string;
    idcarga: any;
    tipoclases: string;
    idrespuesta?: number;
}
export class CursosListModel {
    coddocente:string;
    docente:string; 
    sigla:string;
    curso:string;
    modalidad:string;
    idcarga:any;
}