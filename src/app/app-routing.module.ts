import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { AuthGuard } from './core/auth.guard';
import { AccesoComponent } from './shared/acceso/acceso.component';


const routes: Routes = [
  {
    path: '',
    component: AccesoComponent
  },
  {
    path: 'Login',
    component: AccesoComponent
  },
  {
    path: 'Sistemas',
    component: PagesComponent,
    children:
      [
        {
          path: 'Encuesta',
          loadChildren: () => import('./pages/encuesta/encuesta.module').then(m => m.EncuestaModule)
          // loadChildren: './pages/encuesta/encuesta.module#EncuestaModule'
        },
        {
          path: 'Desarrollo',
          loadChildren: () => import('./pages/desarrollo/desarrollo.module').then(m => m.DesarrolloModule),          
          // loadChildren: './pages/desarrollo/desarrollo.module#DesarrolloModule',
          canActivate: [AuthGuard]
        },
        {
          path: 'Resultados',
          loadChildren: () => import('./pages/resultados/resultados.module').then(m => m.ResultadosModule),
          // loadChildren: './pages/resultados/resultados.module#ResultadosModule',
          canActivate: [AuthGuard]
        },
        {
          path: 'Mantenimientos',
          loadChildren: () => import('./pages/mantenimientos/mantenimientos.module').then(m => m.MantenimientosModule),
          // loadChildren: './pages/mantenimientos/mantenimientos.module#MantenimientosModule',
          canActivate: [AuthGuard]
        }

      ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
